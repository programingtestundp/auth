import 'package:auth/state/auth/models/auth_result.dart';
import 'package:auth/state/auth/providers/auth_state_notifier.dart';
import 'package:auth/state/user_info/models/user.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../initial_firebase_mock.dart';
import '../arranges/arrange.dart';
import '../models/status.dart';
import '../models/with_google_model.dart';

void main() async {
  setupFirebaseAuthMocks();
  setUpAll(() async {
    await Firebase.initializeApp();
  });
  //? ▶ test 1: Sign up success  ◀ //
  group('Sign up success ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.success);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test(
        'login with google successfully done and return with UserCredential and stop loading',
        () async {
      await resultTest(mockWithGoogle);
    });
  });

  //? ▶ test 3: Sign In With Credential Exception  ◀ //
  group('Sign up Exception ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle =
          MockWithGoogle(TestStatus.signUpWithEmailPasswordException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test(
        'login with google fail because exception signInWithCredential and stop loading',
        () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
  });

  //? ▶ test 4: If th current user of firebase is null ◀ //
  group('If th current user of firebase is null ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.currentUserIsNull);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test(
        'login with google fail because exception currentUser is null and stop loading',
        () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
  });

  //? ▶ test 6: fail in get Users bu email ◀ //
  group('fail in get Users by email ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getUserByEmailException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test(
        'login with google fail because exception on get Users collection and stop loading',
        () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
  });

  //? ▶ test 12: drop user in sql Exception ◀ //
  group('Error: drop user in sql ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.dropUserInSqlException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
  });
  //? ▶ test 13: create user in sql Exception ◀ //
  group('Error: create user in sql  ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.dropUserInSqlException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
  });
}

void startTest(MockWithGoogle mockWithGoogle) {
  // ▶ act ◀ //
  final state = mockWithGoogle.container.read(authNotifierProvider);
  // ▶ assert ◀ //
  expect(state.isLoading, false);
}

void signInTest(MockWithGoogle mockWithGoogle) {
  // ▶ arrange ◀ //
  arrange(mockWithGoogle, type: ArrangeType.signUp);
  // ▶ act ◀ //
  mockWithGoogle.loginResult = mockWithGoogle.container
      .read(authNotifierProvider.notifier)
      .signUp(
          passWord: 'password',
          user: UserModel(
              email: 'email@gmail.com',
              birthDay: '2000/01//02',
              phone: Phone(number: '2124567890')));
  final state = mockWithGoogle.container.read(authNotifierProvider);

  // ▶ assert ◀ //
  expect(state.isLoading, true);
}

Future<void> resultTest(MockWithGoogle mockWithGoogle,
    {bool isFail = false}) async {
  await mockWithGoogle.loginResult;
  // ▶ assert ◀ //
  final state = mockWithGoogle.container.read(authNotifierProvider);
  if (!isFail) {
    expect(state.user, isNotNull);
    expect(state.result, AuthResults.success);
    expect(state.isLoading, false);
  } else {
    expect(state.user, isNull);
    expect(state.result, AuthResults.failure);
    expect(state.isLoading, false);
  }
}
