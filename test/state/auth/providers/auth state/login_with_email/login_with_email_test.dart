import 'package:auth/state/auth/models/auth_result.dart';
import 'package:auth/state/auth/providers/auth_state_notifier.dart';
import 'package:auth/state/user_info/models/user.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../initial_firebase_mock.dart';
import '../arranges/arrange.dart';
import '../models/status.dart';
import '../models/with_google_model.dart';

void main() async {
  setupFirebaseAuthMocks();
  setUpAll(() async {
    await Firebase.initializeApp();
  });
  //? ▶ test 1: Sign in success  ◀ //
  group('Sign in success ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.success);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test(
        'login with google successfully done and return with UserCredential and stop loading',
        () async {
      await resultTest(mockWithGoogle);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });

  //? ▶ test 3: Sign In With Credential Exception  ◀ //
  group('Sign In With Credential Exception ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle =
          MockWithGoogle(TestStatus.signInWithEmailPasswordException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test(
        'login with google fail because exception signInWithCredential and stop loading',
        () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });

  //? ▶ test 4: If th current user of firebase is null ◀ //
  group('If th current user of firebase is null ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.currentUserIsNull);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test(
        'login with google fail because exception currentUser is null and stop loading',
        () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });

  //? ▶ test 5: fail in check if the email already register in fire store collection ◀ //
  // group(
  //     'fail in check if the email already register in fire store collection ~',
  //     () {
  //   late MockWithGoogle mockWithGoogle;

  //   setUpAll(() {
  //     mockWithGoogle = MockWithGoogle(TestStatus.checkEmailException);
  //   });
  //   test('start with no loading', () {
  //     startTest(mockWithGoogle);
  //   });
  //   test('google sign in', () {
  //     signInTest(mockWithGoogle);
  //   });
  //   test(
  //       'login with google fail because exception on check the email collection and stop loading',
  //       () async {
  //     await resultTest(mockWithGoogle, isFail: true);
  //   });
  //   tearDownAll(() {
  //     mockWithGoogle.container.dispose();
  //   });
  // });

  //? ▶ test 6: fail in get Users bu email ◀ //
  group('fail in get Users by email ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getUserByEmailException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test(
        'login with google fail because exception on get Users collection and stop loading',
        () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
  //? ▶ test 7:  fail in get Users ◀ //
  group('user is not found ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getUserByEmailNotFound);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
  //? ▶ test 8:error with user's data  ◀ //
  group('error with user\'s data ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getUserByEmailWrongData);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });

  //? ▶ test 9:fain in get Phone model for users  ◀ //
  group('fail in get Phone model for users ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getPhoneByEmailException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
  //? ▶ test 10:Error: there is no phone for this user  ◀ //
  group('Error: there is no phone for this user ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getPhoneByIdNotFound);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
  //? ▶ test 11: Error in Pone data  ◀ //
  group('Error in Pone data ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getPhoneByIdWrongData);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });

  //? ▶ test 9:fain in get Address model for users  ◀ //
  group('fail in get Address model for users ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getAddressByIdException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
  //? ▶ test 10:Error: there is no Address for this user  ◀ //
  group('Error: there is no Address for this user ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getAddressByIdNotFound);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
  //? ▶ test 11: Error in Address data  ◀ //
  group('Error in Address data ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.getAddressByIdWrongData);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
  //? ▶ test 12: drop user in sql Exception ◀ //
  group('Error: drop user in sql ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.dropUserInSqlException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
  //? ▶ test 13: create user in sql Exception ◀ //
  group('Error: create user in sql  ~', () {
    late MockWithGoogle mockWithGoogle;

    setUpAll(() {
      mockWithGoogle = MockWithGoogle(TestStatus.dropUserInSqlException);
    });
    test('start with no loading', () {
      startTest(mockWithGoogle);
    });
    test('google sign in', () {
      signInTest(mockWithGoogle);
    });
    test('login with google fail and stop loading', () async {
      await resultTest(mockWithGoogle, isFail: true);
    });
    tearDownAll(() {
      mockWithGoogle.container.dispose();
    });
  });
}

void startTest(MockWithGoogle mockWithGoogle) {
  // ▶ act ◀ //
  final state = mockWithGoogle.container.read(authNotifierProvider);
  // ▶ assert ◀ //
  expect(state.isLoading, false);
}

void signInTest(MockWithGoogle mockWithGoogle) {
  // ▶ arrange ◀ //
  arrange(mockWithGoogle, type: ArrangeType.login);
  // ▶ act ◀ //
  mockWithGoogle.loginResult = mockWithGoogle.container
      .read(authNotifierProvider.notifier)
      .logInWithEmail(
          passWord: 'password', user: UserModel(email: 'email@gmail.com'));
  final state = mockWithGoogle.container.read(authNotifierProvider);

  // ▶ assert ◀ //
  expect(state.isLoading, true);
}

Future<void> resultTest(MockWithGoogle mockWithGoogle,
    {bool isFail = false}) async {
  await mockWithGoogle.loginResult;
  // ▶ assert ◀ //
  final state = mockWithGoogle.container.read(authNotifierProvider);
  if (!isFail) {
    expect(state.user, isNotNull);
    expect(state.result, AuthResults.success);
    expect(state.isLoading, false);
  } else {
    expect(state.user, isNull);
    expect(state.result, AuthResults.failure);
    expect(state.isLoading, false);
  }
}
