import 'package:auth/state/constants/firebase_collections_names.dart';
import 'package:auth/state/constants/firebase_field_names.dart';
import 'package:auth/state/user_info/DB/constant/query_string.dart';
import 'package:mockito/mockito.dart';

import '../index_test.mocks.dart';
import '../models/status.dart';
import '../models/with_google_model.dart';

part 'firebase/google_sign_in.dart';
part 'firebase/sign_in_with_credential.dart';
part 'firebase/current_user_check.dart';
part 'firebase/check_email.dart';
part 'firebase/add_email.dart';
part 'firebase/get_user.dart';
part 'firebase/add_user.dart';
part 'firebase/phone.dart';
part 'firebase/address.dart';
part 'firebase/sign_in_with_email.dart';
part 'firebase/sign_up.dart';
part 'sql/drop.dart';
part 'sql/create.dart';

enum ArrangeType { google, login, signUp }

void arrange(MockWithGoogle mockWithGoogle,
    {ArrangeType type = ArrangeType.google}) {
  //* ▶ arrange test GoogleSignIn ◀ //
  arrangeGoogleSignIn(mockWithGoogle);
  //* ▶ arrange test firebase SignIn ◀ //
  if (type == ArrangeType.google) {
    arrangeSignInWithCredential(mockWithGoogle);
  } else if (type == ArrangeType.login) {
    arrangeSignInWithEmail(mockWithGoogle);
  } else if (type == ArrangeType.signUp) {
    arrangeSignUpWithEmail(mockWithGoogle);
  }
  //* ▶ arrange check current user ◀ //
  arrangeCurrentUser(mockWithGoogle);

//* ▶ arrange for get user from fire store ◀ //
  arrangeForGetUser(mockWithGoogle);
  //* ▶ arrange for save user to fire store ◀ //
  arrangeForSaveUser(mockWithGoogle);
}

void arrangeForGetUser(MockWithGoogle mockWithGoogle) {
  final MockWithGoogleQuery googleQueryForEmail = mockWithGoogle.mockForEmail;

  // ▶ get user by id ◀ //
  when(googleQueryForEmail.collectionReference.where(
          FirebaseFieldsNamesUsers.email,
          isEqualTo: anyNamed('isEqualTo')))
      .thenReturn(googleQueryForEmail.query);
  // ▶ get user by email ◀ //
  when(googleQueryForEmail.collectionReference.where(
          FirebaseFieldsNamesUsers.email,
          isEqualTo: anyNamed('isEqualTo')))
      .thenReturn(googleQueryForEmail.query);
  when(googleQueryForEmail.query.limit(1))
      .thenReturn(googleQueryForEmail.query);

  //* ▶ arrange for check if the user exists by check the email ◀ //
  arrangeCheckEmail(mockWithGoogle);
  when(googleQueryForEmail.queryDocumentSnapshot.data()).thenReturn({});

  //* ▶ arrange for ad email ◀ //
  arrangeAddEmail(mockWithGoogle);
}

void arrangeForSaveUser(MockWithGoogle mockWithGoogle) {
  final MockWithGoogleQuery googleQueryForGetUserByEmail =
      mockWithGoogle.mockForGetUserByEmail;
  final MockWithGoogleQuery googleQueryForGetPhoneByEmail =
      mockWithGoogle.mockForGetPhoneByEmail;
  final MockWithGoogleQuery googleQueryForGetAddressByEmail =
      mockWithGoogle.mockForGetAddressByEmail;

  when(mockWithGoogle.firebaseFirestore
          .collection(FirebaseCollectionsNames.users))
      .thenReturn(googleQueryForGetUserByEmail.collectionReference);
  when(googleQueryForGetUserByEmail.collectionReference.where(
          FirebaseFieldsNamesUsers.email,
          isEqualTo: anyNamed('isEqualTo')))
      .thenReturn(googleQueryForGetUserByEmail.query);
  when(googleQueryForGetUserByEmail.query.limit(1))
      .thenReturn(googleQueryForGetUserByEmail.query);

  // //* ▶ arrange get users ◀ //
  // arrangeGetUserById(mockWithGoogle);
  //* ▶ arrange get user by email ◀ //
  arrangeGetUserBy(mockWithGoogle);
  when(googleQueryForGetUserByEmail.queryDocumentSnapshot.reference)
      .thenReturn(googleQueryForGetUserByEmail.documentReference);
  arrangeAddUser(mockWithGoogle);

  //* ▶ arrange deal with phone ◀ //
  when(googleQueryForGetUserByEmail.documentReference
          .collection(FirebaseCollectionsNames.phone))
      .thenReturn(googleQueryForGetPhoneByEmail.collectionReference);
  arrangePhoneGet(mockWithGoogle);
  when(googleQueryForGetPhoneByEmail.queryDocumentSnapshot.reference)
      .thenReturn(googleQueryForGetUserByEmail.documentReference);
  arrangePhoneAdd(mockWithGoogle);

  //* ▶ arrange deal with Address ◀ //
  when(googleQueryForGetUserByEmail.documentReference
          .collection(FirebaseCollectionsNames.address))
      .thenReturn(googleQueryForGetAddressByEmail.collectionReference);
  arrangeAddressGet(mockWithGoogle);
  when(googleQueryForGetAddressByEmail.queryDocumentSnapshot.reference)
      .thenReturn(googleQueryForGetAddressByEmail.documentReference);
  arrangeAddressAdd(mockWithGoogle);

  //* ▶ arrange for save in SQL DB ◀ //
  arrangeCreateUserFromSql(mockWithGoogle);
  arrangeDropUserFromSql(mockWithGoogle);
}
