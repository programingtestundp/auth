part of '../arrange.dart';

void arrangeCreateUserFromSql(MockWithGoogle mockWithGoogle) {
  final googleQuerySql = mockWithGoogle.database;
  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //
    TestStatus.createUserInSqlException =>
      when(googleQuerySql.execute(SQLString.createUser))
          .thenThrow(mockWithGoogle.getDatabaseException()),

    // ▶ case 2 (success) ◀ //
    _ => () {}.call()
  };
}
