part of '../arrange.dart';

void arrangeDropUserFromSql(MockWithGoogle mockWithGoogle) {
  final googleQuerySql = mockWithGoogle.database;
  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //
    TestStatus.dropUserInSqlException =>
      when(googleQuerySql.execute(SQLString.dropUser))
          .thenThrow(mockWithGoogle.getDatabaseException()),

    // ▶ case 2 (success) ◀ //
    _ => () {}.call()
  };
}
