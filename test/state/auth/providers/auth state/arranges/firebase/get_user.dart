part of '../arrange.dart';

// void arrangeGetUser(MockWithGoogle mockWithGoogle) {
//   final googleQuery = mockWithGoogle.mockForSaveUser;

//   return switch (mockWithGoogle.status) {
//     // ▶ case 1 ◀ //
//     TestStatus.getUserException => when(googleQuery.query.get()).thenThrow(
//         mockWithGoogle.getFirebaseAuthException(
//             message: 'Exception with grt user from the fire store')),

//     // ▶ case 2 ◀ //
//     TestStatus.getUserByIdNotFound => () {
//         when(googleQuery.query.get())
//             .thenAnswer((_) async => googleQuery.querySnapshot);
//         when(googleQuery.querySnapshot.docs).thenReturn([]);
//       }.call(),

//     // ▶ case 3 ◀ //
//     TestStatus.getUserByIdWrongData => () {
//         when(googleQuery.query.get())
//             .thenAnswer((_) async => googleQuery.querySnapshot);
//         when(googleQuery.querySnapshot.docs)
//             .thenReturn([googleQuery.queryDocumentSnapshot]);
//         when(googleQuery.queryDocumentSnapshot.data()).thenThrow(
//             mockWithGoogle.getException(
//                 message: 'Exception with grt user data from the fire store'));
//       }.call(),

//     // ▶ case 4 (success) ◀ //
//     _ => () {
//         when(googleQuery.query.get())
//             .thenAnswer((_) async => googleQuery.querySnapshot);
//         when(googleQuery.querySnapshot.docs)
//             .thenReturn([googleQuery.queryDocumentSnapshot]);
//         when(googleQuery.queryDocumentSnapshot.data())
//             .thenReturn(googleQuery.userModel);
//       }.call()
//   };
// }

void arrangeGetUserBy(
  MockWithGoogle mockWithGoogle,
) {
  final googleQuery = mockWithGoogle.mockForGetUserByEmail;

  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //

    TestStatus.getUserByEmailException => when(googleQuery.query.get())
        .thenThrow(mockWithGoogle.getFirebaseException(
            message: 'Exception with grt user from the fire store')),
    // ▶ case 2 ◀ //

    TestStatus.getUserByEmailNotFound => () {
        when(googleQuery.query.get())
            .thenAnswer((_) async => googleQuery.querySnapshot);
        when(googleQuery.querySnapshot.docs).thenReturn([]);
      }.call(),

    // ▶ case 3 (success) ◀ //
    TestStatus.getUserByEmailWrongData => () {
        when(googleQuery.query.get())
            .thenAnswer((_) async => googleQuery.querySnapshot);
        when(googleQuery.querySnapshot.docs)
            .thenReturn([googleQuery.queryDocumentSnapshot]);
        when(googleQuery.queryDocumentSnapshot.data()).thenThrow(
            mockWithGoogle.getException(
                message: 'Exception with grt User data from the fire store'));
      }.call(),
    // ▶ case 3 (success) ◀ //
    _ => () {
        when(googleQuery.query.get())
            .thenAnswer((_) async => googleQuery.querySnapshot);
        when(googleQuery.querySnapshot.docs)
            .thenReturn([googleQuery.queryDocumentSnapshot]);
        when(googleQuery.queryDocumentSnapshot.data())
            .thenReturn(googleQuery.userModel);
      }.call()
  };
}
