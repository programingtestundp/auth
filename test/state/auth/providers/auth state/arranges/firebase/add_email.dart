part of '../arrange.dart';

void arrangeAddEmail(MockWithGoogle mockWithGoogle) {
  final googleQueryForEmail = mockWithGoogle.mockForEmail;
  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //
    TestStatus.addEmailException => when(googleQueryForEmail.collectionReference
          .add({
        FirebaseFieldsNamesUsers.email: googleQueryForEmail.userModel.email
      })).thenThrow(mockWithGoogle.getFirebaseAuthException(
          message: 'Exception with add email')),

    // ▶ case 2 (success) ◀ //
    _ => when(googleQueryForEmail.collectionReference.add({
        FirebaseFieldsNamesUsers.email: googleQueryForEmail.userModel.email
      })).thenAnswer((_) async => googleQueryForEmail.documentReference)
  };
}
