part of '../arrange.dart';

void arrangeSignUpWithEmail(MockWithGoogle mockWithGoogle) =>
    switch (mockWithGoogle.status) {
      // ▶ case 1 ◀ //
      TestStatus.signUpWithEmailPasswordException => when(
              mockWithGoogle.firebaseAuth.createUserWithEmailAndPassword(
                  email: anyNamed('email'), password: anyNamed('password')))
          .thenThrow(mockWithGoogle.getFirebaseAuthException(
              message: 'SignInWithCredential Exception')),

      // ▶ case 2 (success) ◀ //
      _ => when(mockWithGoogle.firebaseAuth.createUserWithEmailAndPassword(
              email: anyNamed('email'), password: anyNamed('password')))
          .thenAnswer((_) async => mockWithGoogle.userCredential)
    };
