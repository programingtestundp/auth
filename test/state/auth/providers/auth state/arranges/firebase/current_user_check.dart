part of '../arrange.dart';

void arrangeCurrentUser(MockWithGoogle mockWithGoogle) =>
    switch (mockWithGoogle.status) {
      // ▶ case 1 ◀ //
      TestStatus.currentUserIsNull =>
        when(mockWithGoogle.firebaseAuth.currentUser).thenReturn(null),

      // ▶ case 2 (success) ◀ //
      _ => when(mockWithGoogle.firebaseAuth.currentUser).thenReturn(MockUser())
    };
