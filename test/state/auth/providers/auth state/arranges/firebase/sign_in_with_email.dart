part of '../arrange.dart';

void arrangeSignInWithEmail(MockWithGoogle mockWithGoogle) =>
    switch (mockWithGoogle.status) {
      // ▶ case 1 ◀ //
      TestStatus.signInWithEmailPasswordException => when(
              mockWithGoogle.firebaseAuth.signInWithEmailAndPassword(
                  email: anyNamed('email'), password: anyNamed('password')))
          .thenThrow(mockWithGoogle.getFirebaseAuthException(
              message: 'SignInWithCredential Exception')),

      // ▶ case 2 (success) ◀ //
      _ => when(mockWithGoogle.firebaseAuth.signInWithEmailAndPassword(
              email: anyNamed('email'), password: anyNamed('password')))
          .thenAnswer((_) async => mockWithGoogle.userCredential)
    };
