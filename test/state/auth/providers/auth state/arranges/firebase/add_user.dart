part of '../arrange.dart';

void arrangeAddUser(MockWithGoogle mockWithGoogle) {
  final googleQuery = mockWithGoogle.mockForGetUserByEmail;
  final googleQueryForIdInstance = mockWithGoogle.mockForGetUserByEmail;
  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //
    TestStatus.addUserException => () {
        when(googleQuery.collectionReference.add(googleQuery.userModel))
            .thenThrow(mockWithGoogle.getFirebaseAuthException(
                message: 'Exception with add user'));

        when(googleQueryForIdInstance.collectionReference
                .add(googleQuery.userModel))
            .thenThrow(mockWithGoogle.getFirebaseAuthException(
                message: 'Exception with add user'));
      }.call(),

    // ▶ case 2 (success) ◀ //
    _ => () {
        when(googleQuery.collectionReference.add(googleQuery.userModel))
            .thenAnswer((_) async => googleQuery.documentReference);

        when(googleQueryForIdInstance.collectionReference
                .add(googleQuery.userModel))
            .thenAnswer(
                (_) async => googleQueryForIdInstance.documentReference);
      }.call()
  };
}
