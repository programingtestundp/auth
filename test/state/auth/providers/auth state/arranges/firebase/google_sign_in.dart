part of '../arrange.dart';

void arrangeGoogleSignIn(MockWithGoogle mockWithGoogle) =>
    switch (mockWithGoogle.status) {
      // ▶ case 1 ◀ //
      TestStatus.googleSignInError => when(mockWithGoogle.googleSignIn.signIn())
          .thenAnswer((_) async => null),

      // ▶ case 2 (success) ◀ //
      _ => when(mockWithGoogle.googleSignIn.signIn())
          .thenAnswer((_) async => mockWithGoogle.googleSignInAccount)
    };
