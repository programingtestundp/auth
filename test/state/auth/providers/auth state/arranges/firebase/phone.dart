part of '../arrange.dart';

void arrangePhoneGet(MockWithGoogle mockWithGoogle) {
  final MockWithGoogleQuery googleQuery = mockWithGoogle.mockForGetUserByEmail;

  final MockWithGoogleQuery googleQueryPhone =
      mockWithGoogle.mockForGetPhoneByEmail;
  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //
    TestStatus.getPhoneByEmailException =>
      when(googleQueryPhone.collectionReference.get()).thenThrow(mockWithGoogle
          .getFirebaseAuthException(message: 'Exception with get Phone')),

    // ▶ case 2 ◀ //
    TestStatus.getPhoneByIdNotFound => () {
        when(googleQueryPhone.collectionReference.get())
            .thenAnswer((_) async => googleQueryPhone.querySnapshot);
        when(googleQueryPhone.querySnapshot.docs).thenReturn([]);
      }.call(),

    // ▶ case 3 ◀ //
    TestStatus.getPhoneByIdWrongData => () {
        when(googleQueryPhone.collectionReference.get())
            .thenAnswer((_) async => googleQueryPhone.querySnapshot);
        when(googleQueryPhone.querySnapshot.docs)
            .thenReturn([googleQueryPhone.queryDocumentSnapshot]);
        when(googleQueryPhone.queryDocumentSnapshot.data()).thenThrow(
            mockWithGoogle.getException(
                message: 'Exception with grt phone data from the fire store'));
      }.call(),

    // ▶ case 4 (success) ◀ //
    _ => () {
        when(googleQueryPhone.collectionReference.get())
            .thenAnswer((_) async => googleQueryPhone.querySnapshot);
        when(googleQueryPhone.querySnapshot.docs)
            .thenReturn([googleQueryPhone.queryDocumentSnapshot]);
        when(googleQueryPhone.queryDocumentSnapshot.data())
            .thenReturn(googleQuery.userModel.phone);
      }.call(),
  };
}

void arrangePhoneAdd(MockWithGoogle mockWithGoogle) {
  final MockWithGoogleQuery googleQuery = mockWithGoogle.mockForGetUserByEmail;

  final MockWithGoogleQuery googleQueryPhone =
      mockWithGoogle.mockForGetPhoneByEmail;
  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //
    TestStatus.addPhoneException => when(googleQueryPhone.collectionReference
            .add(googleQuery.userModel.phone))
        .thenThrow(mockWithGoogle.getFirebaseAuthException(
            message: 'Exception with get Phone')),

    // ▶ case 2 (success) ◀ //
    _ => when(googleQueryPhone.collectionReference
            .add(googleQuery.userModel.phone))
        .thenAnswer((_) async => googleQueryPhone.documentReference)
  };
}
