part of '../arrange.dart';

void arrangeCheckEmail(MockWithGoogle mockWithGoogle) =>
    switch (mockWithGoogle.status) {
      // ▶ case 1 ◀ //
      TestStatus.checkEmailException =>
        when(mockWithGoogle.mockForEmail.query.get()).thenThrow(mockWithGoogle
            .getFirebaseException(message: 'Exception with check email')),

      // ▶ case 3 ◀ //
      TestStatus.checkEmailNotFound => () {
          when(mockWithGoogle.mockForEmail.query.get()).thenAnswer(
              (_) async => mockWithGoogle.mockForEmail.querySnapshot);
          when(mockWithGoogle.mockForEmail.querySnapshot.docs).thenReturn([]);
        }.call(),

      // ▶ case 3 (success) ◀ //
      _ => () {
          when(mockWithGoogle.mockForEmail.query.get()).thenAnswer(
              (_) async => mockWithGoogle.mockForEmail.querySnapshot);
          when(mockWithGoogle.mockForEmail.querySnapshot.docs)
              .thenReturn([mockWithGoogle.mockForEmail.queryDocumentSnapshot]);
        }.call()
    };
