part of '../arrange.dart';

void arrangeSignInWithCredential(MockWithGoogle mockWithGoogle) =>
    switch (mockWithGoogle.status) {
      // ▶ case 1 ◀ //
      TestStatus.signInWithCredentialException => when(mockWithGoogle
              .firebaseAuth
              .signInWithCredential(mockWithGoogle.oAuthCredential))
          .thenThrow(mockWithGoogle.getFirebaseAuthException(
              message: 'SignInWithCredential Exception')),

      // ▶ case 2 (success) ◀ //
      _ => when(mockWithGoogle.firebaseAuth
              .signInWithCredential(mockWithGoogle.oAuthCredential))
          .thenAnswer((_) async => mockWithGoogle.userCredential)
    };
