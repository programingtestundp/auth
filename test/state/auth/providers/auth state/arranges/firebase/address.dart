part of '../arrange.dart';

void arrangeAddressGet(MockWithGoogle mockWithGoogle) {
  final MockWithGoogleQuery googleQuery = mockWithGoogle.mockForGetUserByEmail;

  final MockWithGoogleQuery googleQueryAddress =
      mockWithGoogle.mockForGetAddressByEmail;
  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //
    TestStatus.getAddressByIdException =>
      when(googleQueryAddress.collectionReference.get()).thenThrow(
          mockWithGoogle.getFirebaseAuthException(
              message: 'Exception with get Address')),

    // ▶ case 2 ◀ //
    TestStatus.getAddressByIdNotFound => () {
        when(googleQueryAddress.collectionReference.get())
            .thenAnswer((_) async => googleQueryAddress.querySnapshot);
        when(googleQueryAddress.querySnapshot.docs).thenReturn([]);
      }.call(),

    // ▶ case 3 ◀ //
    TestStatus.getAddressByIdWrongData => () {
        when(googleQueryAddress.collectionReference.get())
            .thenAnswer((_) async => googleQueryAddress.querySnapshot);
        when(googleQueryAddress.querySnapshot.docs)
            .thenReturn([googleQueryAddress.queryDocumentSnapshot]);
        when(googleQueryAddress.queryDocumentSnapshot.data()).thenThrow(
            mockWithGoogle.getException(
                message:
                    'Exception with grt address data from the fire store'));
      }.call(),

    // ▶ case 4 (success) ◀ //
    _ => () {
        when(googleQueryAddress.collectionReference.get())
            .thenAnswer((_) async => googleQueryAddress.querySnapshot);
        when(googleQueryAddress.querySnapshot.docs)
            .thenReturn([googleQueryAddress.queryDocumentSnapshot]);
        when(googleQueryAddress.queryDocumentSnapshot.data())
            .thenReturn(googleQuery.userModel.address);
      }.call(),
  };
}

void arrangeAddressAdd(MockWithGoogle mockWithGoogle) {
  final MockWithGoogleQuery googleQuery = mockWithGoogle.mockForGetUserByEmail;

  final MockWithGoogleQuery googleQueryAddress =
      mockWithGoogle.mockForGetAddressByEmail;
  return switch (mockWithGoogle.status) {
    // ▶ case 1 ◀ //
    TestStatus.addAddressException => when(googleQueryAddress
            .collectionReference
            .add(googleQuery.userModel.address))
        .thenThrow(mockWithGoogle.getFirebaseAuthException(
            message: 'Exception with get Address')),

    // ▶ case 2 (success) ◀ //
    _ => when(googleQueryAddress.collectionReference
            .add(googleQuery.userModel.address))
        .thenAnswer((_) async => googleQueryAddress.documentReference)
  };
}
