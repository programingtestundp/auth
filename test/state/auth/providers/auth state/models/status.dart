enum TestStatus {
  success,
  googleSignInError,
  signInWithCredentialException,
  signInWithEmailPasswordException,
  signUpWithEmailPasswordException,
  currentUserIsNull,
  checkEmailException,
  checkEmailNotFound,
  addEmailException,
  addUserException,

  getUserByEmailException,
  getUserByEmailNotFound,
  getUserByEmailWrongData,

  getPhoneByEmailException,
  addPhoneException,
  getPhoneByIdNotFound,
  getPhoneByIdWrongData,
  getAddressByIdException,
  addAddressException,
  getAddressByIdNotFound,
  getAddressByIdWrongData,

  dropUserInSqlException,
  createUserInSqlException,
}
