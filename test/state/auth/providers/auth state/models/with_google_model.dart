import 'package:auth/state/auth/models/auth_result.dart';
import 'package:auth/state/auth/providers/auth_state_notifier.dart';
import 'package:auth/state/user_info/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:sqflite/sqflite.dart';

import '../index_test.mocks.dart';
import 'status.dart';

class MockWithGoogle {
  late MockGoogleSignIn googleSignIn;
  late MockGoogleSignInAccount googleSignInAccount;
  late Future<AuthResults> loginResult;
  late ProviderContainer container;
  late MockFirebaseAuth firebaseAuth;
  late MockOAuthCredential oAuthCredential;
  late MockFirebaseFirestore firebaseFirestore;
  late MockWithGoogleQuery mockForEmail;
  late MockWithGoogleQuery mockForGetUserByEmail;
  late MockWithGoogleQuery mockForGetPhoneByEmail;
  late MockWithGoogleQuery mockForGetAddressByEmail;

  late MockDatabase database;
  late MockUserCredential userCredential;

  final TestStatus status;
  MockWithGoogle(this.status) {
    firebaseFirestore = MockFirebaseFirestore();
    googleSignIn = MockGoogleSignIn();
    googleSignInAccount = MockGoogleSignInAccount();
    firebaseAuth = MockFirebaseAuth();
    oAuthCredential = MockOAuthCredential();
    database = MockDatabase();
    container = ProviderContainer(overrides: [
      authNotifierProvider.overrideWith(() => AuthNotifier.test(
          fAuth: firebaseAuth,
          fireStore: firebaseFirestore,
          database: database))
    ]);
    mockForEmail = MockWithGoogleQuery();

    mockForGetUserByEmail = MockWithGoogleQuery();
    mockForGetPhoneByEmail = MockWithGoogleQuery();
    mockForGetAddressByEmail = MockWithGoogleQuery();

    userCredential = MockUserCredential();
  }

  FirebaseAuthException getFirebaseAuthException({String? message}) =>
      FirebaseAuthException(
        code: 'operation-not-allowed',
        message: message ?? 'Anonymous auth not enabled.',
      );
  FirebaseException getFirebaseException({String? message}) =>
      FirebaseException(
        plugin: 'firestore',
        code: 'operation-not-allowed',
        message: message ?? 'Anonymous auth not enabled.',
      );
  Exception getException({String? message}) => Exception(
        message ?? 'Anonymous auth not enabled.',
      );
  DatabaseException getDatabaseException({String? message}) =>
      MockDatabaseException();
}

class MockWithGoogleQuery {
  late MockCollectionReference collectionReference;
  late MockQuery query;
  late MockQuerySnapshot querySnapshot;
  late MockQueryDocumentSnapshot queryDocumentSnapshot;
  late MockDocumentReference documentReference;
  late UserModel userModel;

  MockWithGoogleQuery() {
    collectionReference = MockCollectionReference();
    querySnapshot = MockQuerySnapshot();
    queryDocumentSnapshot = MockQueryDocumentSnapshot();
    documentReference = MockDocumentReference();
    userModel = UserModel();
    query = MockQuery();
  }
}
