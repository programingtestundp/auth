import 'package:auth/state/auth/models/auth_result.dart';
import 'package:auth/state/auth/providers/auth_state_notifier.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../../initial_firebase_mock.dart';
import '../index_test.mocks.dart';
import 'anonymously_model.dart';

void main() {
  setupFirebaseAuthMocks();
  setUpAll(() async {
    await Firebase.initializeApp();
  });
  // ? ▶ test signInAnonymously ~ success ◀ //
  group('login successfully and returns a UserCredential ~ ', () {
    late MockAnonymously mockAnonymously;

    setUpAll(() async {
      mockAnonymously = MockAnonymously();
    });

    test('start with no loading', () {
      // ▶ act ◀ //
      final state = mockAnonymously.container.read(authNotifierProvider);
      // ▶ assert ◀ //
      expect(state.isLoading, false);
    });
    test('log in anonymously and start loading', () {
      // ▶ arrange ◀ //
      when(mockAnonymously.mockFirebaseAuth.signInAnonymously())
          .thenAnswer((response) async => MockUserCredential());
      // ▶ act ◀ //
      mockAnonymously.loginResult = mockAnonymously.container
          .read(authNotifierProvider.notifier)
          .loginAnonymously();
      final state = mockAnonymously.container.read(authNotifierProvider);

      // ▶ assert ◀ //
      expect(state.isLoading, true);
    });
    test(
        'log in anonymously successfully done and return with UserCredential and stop loading',
        () async {
      await mockAnonymously.loginResult;
      // ▶ assert ◀ //
      final state = mockAnonymously.container.read(authNotifierProvider);
      expect(state.user, isNull);
      expect(state.result, AuthResults.success);
      expect(state.isLoading, false);
    });
  });

  //? ▶ test signInAnonymously ~ fail ◀ //
  group('login fail and returns an exception ~ ', () {
    late MockAnonymously mockAnonymously;

    setUpAll(() async {
      mockAnonymously = MockAnonymously();
    });

    test('start with no loading', () {
      // ▶ act ◀ //
      final state = mockAnonymously.container.read(authNotifierProvider);
      // ▶ assert ◀ //
      expect(state.isLoading, false);
    });
    test('start with no loading', () async {
      // ▶ act ◀ //
      await mockAnonymously.container
          .read(authNotifierProvider.notifier)
          .loginAnonymously();
      // ▶ assert ◀ //
    });
    test('login anonymously and start loading  ', () async {
      // ▶ arrange ◀ //
      when(mockAnonymously.mockFirebaseAuth.signInAnonymously())
          .thenThrow(FirebaseAuthException(
        code: 'operation-not-allowed',
        message: 'Anonymous auth not enabled.',
      ));
      // ▶ act ◀ //
      mockAnonymously.loginResult = mockAnonymously.container
          .read(authNotifierProvider.notifier)
          .loginAnonymously();
      var state = mockAnonymously.container.read(authNotifierProvider);

      // ▶ assert ◀ //
      expect(state.isLoading, true);
    });
    test('login anonymously failed and return exception and stop loading',
        () async {
      await mockAnonymously.loginResult;
      // ▶ assert ◀ //
      final state = mockAnonymously.container.read(authNotifierProvider);
      expect(state.user, isNull);
      expect(state.result, AuthResults.failure);
      expect(state.isLoading, false);
    });
  });
}
