// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:auth/state/auth/models/auth_result.dart';
import 'package:auth/state/auth/providers/auth_state_notifier.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../index_test.mocks.dart';

class MockAnonymously {
  late Future<AuthResults> loginResult;
  late ProviderContainer container;
  final MockFirebaseAuth mockFirebaseAuth = MockFirebaseAuth();

  MockAnonymously() {
    container = ProviderContainer(overrides: [
      authNotifierProvider
          .overrideWith(() => AuthNotifier.test(fAuth: mockFirebaseAuth))
    ]);
  }
}
