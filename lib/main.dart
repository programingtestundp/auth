import 'package:auth/extensions/context/index.dart';
import 'package:auth/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:auto_route/auto_route.dart';

import 'routes/app_router.dart';
import 'state/providers/initial_app.dart';
import 'utils/l10n/l10n.dart';
import 'utils/ui/custom_themes.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() async {
  await dotenv.load(fileName: ".env");
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(ProviderScope(
      child: Splash(
    appRouter: AppRouter(),
  )));
}

class Splash extends ConsumerWidget {
  const Splash({super.key, required this.appRouter});
  final AppRouter appRouter;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final initialApp = ref.watch(initialAppProvider);
    return initialApp.when(
        data: (data) => MyApp(appRouter: appRouter),
        error: (error, stackTrace) => MaterialApp(
              home: Scaffold(
                body: Center(
                  child: Text(
                    'initial error:$error',
                    style: context.styles.style1.copyWith(color: Colors.red),
                  ),
                ),
              ),
            ),
        loading: () => const MaterialApp(
              home: Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ));
  }
}

class MyApp extends ConsumerWidget {
  const MyApp({super.key, required this.appRouter});
  // final _observer = MyObserver();
  final AppRouter appRouter;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Locale local = ref.watch(localizationsProvider);
    final locales = ref.read(localizationsProvider.notifier).all;

    return ScreenUtilInit(
        designSize: const Size(360, 640),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp.router(
            localizationsDelegates: const [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            locale: local,
            supportedLocales: locales,
            routerConfig: appRouter.config(
                // navigatorObservers: () => [_observer],
                deepLinkBuilder: (deepLink) {
              if (deepLink.path.startsWith('/products')) {
                // continute with the platfrom link
                return deepLink;
              } else {
                return DeepLink.defaultPath;
                // or DeepLink.path('/')
                // or DeepLink([HomeRoute()])
              }
            }),
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            theme: ThemeData(
                colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
                useMaterial3: true,
                extensions: [CustomColors.blue]),
          );
        });
  }
}
