import 'package:flutter/material.dart' show BuildContext;

import '../../utils/ui/custom_text_styles.dart';

extension TextStyleContext on BuildContext {
  CustomTextStyles get styles => CustomTextStyles(this);
}
