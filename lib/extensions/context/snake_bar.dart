import 'package:flutter/material.dart'
    show
        BuildContext,
        Center,
        Color,
        Colors,
        ScaffoldMessenger,
        SnackBar,
        Text,
        TextStyle;

extension SnakeBarContext on BuildContext {
  void showSnackBar(
      {required String message,
      Color backgroundColor = Colors.black,
      Color textColor = Colors.white,
      int duration = 7000}) {
    ScaffoldMessenger.of(this).showSnackBar(SnackBar(
      duration: Duration(milliseconds: duration),
      content: Center(
        child: Text(
          message,
          style: TextStyle(color: textColor),
        ),
      ),
      backgroundColor: backgroundColor,
    ));
  }
}
