import 'package:flutter/material.dart' show BuildContext, Theme;
import '../../utils/ui/custom_themes.dart';

extension ThemesContext on BuildContext {
  CustomColors get themes => Theme.of(this).extension<CustomColors>()!;
}
