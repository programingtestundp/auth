import 'package:flutter/material.dart'
    show BuildContext, MediaQuery, Orientation;

extension OrientationContext on BuildContext {
  bool get isPortrait =>
      MediaQuery.of(this).orientation == Orientation.portrait;
}
