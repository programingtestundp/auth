import 'package:flutter/material.dart'
    show BuildContext, Directionality, TextDirection;

extension IsLTRContext on BuildContext {
  bool get isLtr => Directionality.of(this) == TextDirection.ltr;
}
