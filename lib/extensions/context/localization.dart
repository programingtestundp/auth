import 'package:flutter/material.dart' show BuildContext;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

extension LocalizationsContext on BuildContext {
  AppLocalizations get loc => AppLocalizations.of(this)!;
}
