extension RemoveExtraSpace on String {
  String removeExtraSpace() {
    String newString = this;
    while (newString.contains('  ')) {
      newString = newString.replaceAll(RegExp(r"  "), " ");
    }
    return newString;
  }
}
