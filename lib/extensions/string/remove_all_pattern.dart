extension RemoveAllPattern on String {
  String removeAll(Iterable<String> values, {String? replacement}) =>
      values.fold(
          this, (result, value) => result.replaceAll(value, replacement ?? ''));
}
