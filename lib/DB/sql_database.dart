import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SQFLiteDatabase {
  Database? _dataBase;
  Future<Database> get database async {
    if (_dataBase != null) {
      return _dataBase!;
    }
    _dataBase = await _initialize();
    return _dataBase!;
  }

  Future<String> get fullPath async =>
      join(await getDatabasesPath(), 'weeky_copon', 'app.db');

  Future<Database> _initialize() async {
    final path = await fullPath;
    // await deleteDatabase(path);
    final database = openDatabase(
      path,
      version: 1,
      singleInstance: true,
      onConfigure: (db) async {
        await db.execute('PRAGMA foreign_keys = ON');
      },
    );
    debugPrint('SQFLiteDatabase~initial');

    return database;
  }

  clear() async {
    await deleteDatabase(await fullPath);
  }
}
