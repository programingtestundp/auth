import 'package:auto_route/auto_route.dart';

import 'app_router.gr.dart';
import 'auth_gaurd.dart';
import 'routs.dart';

//for nested nav
@AutoRouterConfig(
  replaceInRouteName: 'Screen,Route',
)
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: RoutesNames.home,
          initial: true,
          guards: [AuthGuard()],
          page: HomeRoute.page,
          children: [],
        ),
        AutoRoute(path: RoutesNames.registration, page: RegistrationRoute.page)
      ];
}
