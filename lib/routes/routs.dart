import 'package:flutter/foundation.dart' show immutable;

@immutable
class RoutesNames {
  static const String home = '/home';
  static const String registration = '/registration';
}
