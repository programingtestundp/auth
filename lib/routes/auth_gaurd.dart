import 'package:auth/state/auth/providers/auth_state_notifier.dart';
import 'package:auto_route/auto_route.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'routs.dart';

class AuthGuard extends AutoRouteGuard {
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) async {
    final container = ProviderContainer();

    bool loggedIn = FirebaseAuth.instance.currentUser != null ||
        container.read(authNotifierProvider.select((value) => value.user)) !=
            null;
    if (loggedIn) {
      resolver.next(true);
    } else {
      router.pushNamed(RoutesNames.registration);
    }
  }
}
