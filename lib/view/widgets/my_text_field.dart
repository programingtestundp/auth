import 'package:auth/extensions/context/index.dart';
import 'package:auth/view/screens/Auth_Screens/widgets/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart' show useState, HookWidget;

class MyTextFormFieldWed extends HookWidget {
  final Icon? iconPrefixIcon;
  final GlobalKey formKey;
  final bool blockVisibility;
  final String hinText;
  final String errorText;
  final double widthFactor;
  final double hightFactor;
  final Color backgroundColor;
  final bool activeIsObscure;

  final double borderRadius;
  final bool isAuto;
  final FocusNode? focusNode;
  //active the auto after inalide submit
  final bool resetKey;
  final TextEditingController? controller;
  final TextStyle? hintStyle;
  final TextStyle? inputTextStyle;
  final Color focusedBorderColor;
  final Color fillColor;
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;
  MyTextFormFieldWed(
      {super.key,
      this.controller,
      this.iconPrefixIcon,
      this.hinText = '',
      this.widthFactor = 1,
      this.hightFactor = 0.09,
      this.backgroundColor = Colors.transparent,
      this.activeIsObscure = false,
      this.borderRadius = 0,
      this.errorText = '',
      this.validator,
      this.onChanged,
      this.blockVisibility = false,
      GlobalKey? formKey,
      this.isAuto = true,
      this.resetKey = false,
      this.focusNode,
      this.hintStyle,
      this.inputTextStyle,
      this.focusedBorderColor = Colors.blue,
      this.fillColor = Colors.transparent})
      : formKey = formKey ??= GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final double fullWidth = MediaQuery.of(context).size.width;
    final isObscure = useState(true);
    return TextFormField(
      key: formKey,
      maxLines: 1,
      style: inputTextStyle,
      controller: controller,
      obscureText:
          activeIsObscure || blockVisibility == true ? isObscure.value : false,
      autovalidateMode:
          isAuto || resetKey ? AutovalidateMode.onUserInteraction : null,
      onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
      decoration: InputDecoration(
          filled: true,
          fillColor: fillColor,
          contentPadding: EdgeInsets.symmetric(
              horizontal: 10.w, vertical: context.isPortrait ? 15.h : 25.h),
          hintStyle: hintStyle,
          suffixIcon: activeIsObscure == true
              ? IconButton(
                  icon: Icon(
                    isObscure.value ? Icons.visibility : Icons.visibility_off,
                    size: context.isPortrait ? 20.r : 30.r,
                  ),
                  onPressed: () {
                    isObscure.value = !isObscure.value;
                  })
              : null,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              borderSide: BorderSide(color: focusedBorderColor)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              borderSide: BorderSide(color: focusedBorderColor)),
          hintText: hinText,
          prefixIcon: iconPrefixIcon != null
              ? Padding(
                  padding: EdgeInsets.all(fullWidth * 0.01),
                  child: iconPrefixIcon,
                )
              : null),
      validator: validator,
      onChanged: (value) => onChanged?.call(value),
      focusNode: focusNode,
    );
  }
}
