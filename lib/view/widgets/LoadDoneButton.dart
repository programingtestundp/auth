// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:auth/view/screens/Auth_Screens/widgets/index.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart'
    show ConsumerWidget, WidgetRef, ProviderScope;

import '../../state/auth/models/auth_result.dart';
import '../../state/providers/ui providers/button_tate_provider.dart';
import '../../utils/ui/gen/fonts.gen.dart';
import '../../utils/ui/shadows.dart';

class LoadDoneButton extends StatelessWidget {
  const LoadDoneButton(
      {super.key,
      this.activeDoneStatus = false,
      this.stateKey = false,
      this.text = 'LoadDoneButton',
      this.color = Colors.blue,
      this.fontColor = Colors.white,
      this.isSymbol = false,
      this.width = 40,
      this.hight = 20,
      this.fontSize = 7,
      this.underLine = false,
      required this.function,
      this.navigationPath = ''});
  final String navigationPath;
  final bool stateKey;
  final Future<AuthResults> Function(WidgetRef ref) function;
  final String text;
  final Color color;
  final Color fontColor;
  final double width;
  final double hight;
  final double fontSize;
  final bool underLine;
  final bool isSymbol;
  final bool activeDoneStatus;
  @override
  Widget build(
    BuildContext context,
  ) {
    return ProviderScope(
      overrides: [
        buttonStateProvider.overrideWith(() => ButtonState.initial())
      ],
      child: LoadDoneButtonScop(
          width: width,
          hight: hight,
          activeDoneStatus: activeDoneStatus,
          function: function,
          text: text,
          fontColor: fontColor,
          isSymbol: isSymbol,
          fontSize: fontSize,
          underLine: underLine,
          navigationPath: navigationPath,
          color: color),
    );
  }
}

class LoadDoneButtonScop extends ConsumerWidget {
  const LoadDoneButtonScop(
      {super.key,
      required this.activeDoneStatus,
      required this.width,
      required this.hight,
      required this.function,
      required this.text,
      required this.fontColor,
      required this.fontSize,
      required this.underLine,
      required this.color,
      required this.isSymbol,
      required this.navigationPath});
  final String navigationPath;

  final bool isSymbol;
  final double width;
  final double hight;
  final Future<AuthResults> Function(WidgetRef ref) function;
  final String text;
  final Color fontColor;
  final double fontSize;
  final bool underLine;
  final Color color;
  final int duration = 300;
  final bool activeDoneStatus;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final status = ref.watch(buttonStateProvider);
    return InkWell(
      onTap: activeDoneStatus
          ? null
          : () async {
              // ▶ start loading status ◀ //

              ref
                  .read(buttonStateProvider.notifier)
                  .setStatus(status: ButtonStatus.loading, duration: duration);

              final result = await function.call(ref);

// ▶ end loading status with success or cancel or error ◀ //
              ref
                  .read(buttonStateProvider.notifier)
                  .setStatus(results: result, duration: duration);
              if (navigationPath.isNotEmpty && result == AuthResults.success) {
                await Future.delayed(Durations.extralong1);
                if (!context.mounted) return;
                context.router.popUntilRoot();
                context.router.replaceNamed(navigationPath);
              }
            },
      child: AnimatedContainer(
          width: status == ButtonStatus.non || status == ButtonStatus.animate
              ? width
              : hight,
          height: hight,
          duration: Duration(milliseconds: duration),
          curve: Curves.easeIn,
          decoration: BoxDecoration(
              color: activeDoneStatus
                  ? Colors.green.shade300
                  : status == ButtonStatus.done
                      ? Colors.green
                      : status == ButtonStatus.error
                          ? Colors.red
                          : color,
              boxShadow: activeDoneStatus
                  ? null
                  : shadowList(offset: const Offset(.6, 1)),
              borderRadius: BorderRadius.circular(hight / 2)),
          padding: const EdgeInsets.all(6),
          alignment: Alignment.center,
          child: (status == ButtonStatus.done)
              ? _doneWid()
              : status == ButtonStatus.non
                  ? (isSymbol ? _circleWid(hight) : _textWid())
                  : status == ButtonStatus.loading
                      ? _loadingWid()
                      : status == ButtonStatus.error
                          ? _errorWid()
                          : const SizedBox.shrink()),
    );
  }

  _circleWid(double radius) {
    return CircleAvatar(
      radius: radius,
      backgroundColor: Colors.white,
      child: FittedBox(
          fit: BoxFit.fill,
          child: Center(child: _textWid(activeColor: Colors.green.shade300))),
    );
  }

  Text _textWid({Color? activeColor}) {
    return Text(text,
        style: TextStyle(
            fontFamily: FontFamily.lora,
            color: activeDoneStatus ? activeColor ?? fontColor : fontColor,
            fontSize: fontSize,
            fontWeight: FontWeight.bold,
            decoration:
                underLine ? TextDecoration.underline : TextDecoration.none));
  }

  Widget _loadingWid() {
    return SizedBox(
      width: 40.r,
      height: 40.r,
      child: CircularProgressIndicator(
        color: Colors.white,
        strokeWidth: hight * 0.07.r,
      ),
    );
  }

  Icon _doneWid() {
    return Icon(
      Icons.done,
      size: width * 0.05.r,
      color: Colors.white,
    );
  }

  Icon _errorWid() {
    return Icon(
      Icons.close,
      size: width * 0.05.r,
      color: Colors.white,
    );
  }
}
