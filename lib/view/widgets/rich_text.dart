import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../utils/ui/text/base_text.dart';
import '../../utils/ui/text/link_text.dart';

class RichTextWidget extends StatelessWidget {
  const RichTextWidget({super.key, this.styleFotAll, required this.texts});
  final TextStyle? styleFotAll;
  final Iterable<BaseText> texts;
  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        style: styleFotAll,
        children: texts.map(
          (baseText) {
            if (baseText is LinkText) {
              return TextSpan(
                  text: baseText.text,
                  style: baseText.style,
                  recognizer: TapGestureRecognizer()
                    ..onTap = baseText.onTapped);
            } else {
              return TextSpan(
                text: baseText.text,
                style: baseText.style,
              );
            }
          },
        ).toList(),
      ),
    );
  }
}
