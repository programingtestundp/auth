import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../../routes/routs.dart';

@RoutePage()
class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
              onPressed: () =>
                  context.router.pushNamed(RoutesNames.registration),
              child: const Text('Registration')),
        ],
      ),
    );
  }
}
