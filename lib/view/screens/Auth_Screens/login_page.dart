import 'package:auth/extensions/context/index.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget;
import '../../../state/auth/models/auth_result.dart';
import '../../../state/auth/providers/auth_state_notifier.dart';
import '../../../state/providers/ui providers/registration/registration_providers.dart';
import '../../../utils/ui/text/base_text.dart';
import '../../../utils/ui/text/link_text.dart';
import '../../widgets/LoadDoneButton.dart';
import '../../widgets/rich_text.dart';
import 'widgets/email.dart';
import 'widgets/password.dart';

class LoginScreen extends HookWidget {
  const LoginScreen({super.key});

  @override
  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Scaffold(
      body: SafeArea(
        child: Form(
          key: formKey,
          child: Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    //SignIn email $part
                    const EmailWid(),
                    SizedBox(
                      height: 10.h,
                    ),
                    //SignIn password $part
                    const PasswordWid(),
                    SizedBox(
                      height: 10.h,
                    ),
                    //SignIn remember me & forgot pass $part
                    const _RememberForgetWid(),
                    SizedBox(
                      height: 20.h,
                    ),

                    //SignIn login button $part
                    _EmailLogIn(
                      formKey: formKey,
                    ),

                    SizedBox(
                      height: 10.h,
                    ),
                    //Social login
                    const SocialLogIn(),
                    SizedBox(
                      height: 10.h,
                    ),
                    // //if is allready login with facebook

                    const _AlreadyLoggedInWid(),
                    SizedBox(
                      height: 25.h,
                    ),
                    // SignIn Siginup $part
                    const _GoToSignUpWid(),
                    SizedBox(
                      height: 25.h,
                    ),
                    //SignIn Skip $part

                    const _SkipWid(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _RememberForgetWid extends StatelessWidget {
  const _RememberForgetWid({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Expanded(
          flex: 60,
          child: _RememberMeWid(),
        ),
        const Spacer(
          flex: 3,
        ),
        Expanded(
          flex: 47,
          child: TextButton(
            onPressed: () {
              // forgotPasswordDialog();
            },
            child: Text(
              context.loc.forgotPassword,
              style: context.styles.style1.copyWith(
                color: context.themes.secondaryColor,
              ),
            ),
          ),
        )
      ],
    );
  }
}

class _RememberMeWid extends ConsumerWidget {
  const _RememberMeWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final bool isSwitched = ref.watch(rememberMeKeyProvider);

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          width: 40.w,
          child: Switch(
            activeColor: context.themes.secondaryColor,
            value: isSwitched,
            onChanged: (value) =>
                ref.read(rememberMeKeyProvider.notifier).setState = value,
          ),
        ),
        Text(
          context.loc.rememberMe,
          style: TextStyle(
              fontSize: 10.sp,
              color: isSwitched ? context.themes.secondaryColor : Colors.grey,
              fontWeight: isSwitched ? FontWeight.bold : FontWeight.normal),
        ),
      ],
    );
  }
}

class _EmailLogIn extends ConsumerWidget {
  const _EmailLogIn({
    super.key,
    required this.formKey,
  });
  final GlobalKey<FormState> formKey;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logType = ref.watch(
        authNotifierProvider.select((value) => value.user?.logType ?? ''));

    return LoadDoneButton(
      activeDoneStatus: logType == 'email',
      text: context.loc.login,
      width: 150.w,
      hight: context.isPortrait ? 30.h : 50.h,
      fontSize: 10.sp,
      color: context.themes.thirdColor!,
      navigationPath: '/home',
      function: (ref) async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          return ref.read(authNotifierProvider.notifier).logInWithEmail();
        }
        return AuthResults.failure;
      },
    );
  }
}

class SocialLogIn extends ConsumerWidget {
  const SocialLogIn({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logType = ref.watch(
        authNotifierProvider.select((value) => value.user?.logType ?? ''));
    const int numberOfButtons = 2;
    final double width = (340.w) / numberOfButtons - (140.w / numberOfButtons);
    final double height = context.isPortrait ? 30.h : 50.h;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        //facebook Button
        Expanded(
          flex: 1,
          child: Align(
            alignment: Alignment.center,
            child: LoadDoneButton(
              activeDoneStatus: logType == 'facebook',
              text: 'F',
              width: width,
              hight: height,
              fontSize: 10.sp,
              isSymbol: true,
              color: const Color.fromARGB(255, 16, 70, 114),
              function: (ref) async => AuthResults.aborted,
              fontColor: const Color.fromARGB(255, 16, 70, 114),
            ),
          ),
        ),
        //google button

        Expanded(
          flex: 1,
          child: Align(
            alignment: Alignment.center,
            child: LoadDoneButton(
              text: 'G',
              activeDoneStatus: logType == 'google',
              width: width,
              hight: height,
              fontSize: 10.sp,
              isSymbol: true,
              color: Colors.red,
              fontColor: Colors.red,
              navigationPath: '/home',
              function: (ref) async =>
                  ref.read(authNotifierProvider.notifier).logInWithGoogle(),
            ),
          ),
        ),
      ],
    );
  }
}

class _AlreadyLoggedInWid extends ConsumerWidget {
  const _AlreadyLoggedInWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logType = ref.watch(
        authNotifierProvider.select((value) => value.user?.logType ?? ''));

    return logType == ''
        ? const SizedBox.shrink()
        : RichTextWidget(
            texts: [
              BaseText(
                  text: '${context.loc.youAreAlreadyLogInWith('email')}  ',
                  style: context.styles.style1.copyWith(
                    color: context.themes.color4,
                  )),
              LinkText(
                  text: context.loc.skip,
                  onTapped: () {
                    context.router.popUntilRoot();
                    context.router.replaceNamed('/home');
                  },
                  style: context.styles.style1.copyWith(
                    color: context.themes.linkColor,
                  )),
              BaseText(
                  text: context.loc.toContinueOr,
                  style: context.styles.style1.copyWith(
                    color: context.themes.color4,
                  )),
              LinkText(
                  text: context.loc.log_out,
                  onTapped: () async {
                    await ref.read(authNotifierProvider.notifier).logout();
                  },
                  style: context.styles.style1.copyWith(
                    color: context.themes.linkColor,
                  ))
            ],
          );
  }
}

class _GoToSignUpWid extends ConsumerWidget {
  const _GoToSignUpWid();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return RichTextWidget(texts: [
      BaseText(
          text: context.loc.doesNotHaveAccount,
          style: context.styles.style1
              .copyWith(fontSize: 9.sp, color: context.themes.textColor)),
      LinkText(
          text: context.loc.signUp,
          onTapped: () {
            ref.read(switchRegistrationPageProvider.notifier).setState = 1;
          },
          style: context.styles.style1
              .copyWith(fontSize: 11.sp, color: context.themes.linkColor))
    ]);
  }
}

class _SkipWid extends ConsumerWidget {
  const _SkipWid({
    super.key,
  });
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Align(
      alignment: context.isLtr ? Alignment.bottomRight : Alignment.bottomLeft,
      child: InkWell(
          child: Text(
            '${context.loc.skip}>>>',
            style: TextStyle(
                fontSize: 15.sp,
                color: context.themes.linkColor,
                fontWeight: FontWeight.bold),
          ),
          onTap: () async {
            context.router.popUntilRoot();
            context.router.replaceNamed('/home');
          }),
    );
  }
}
