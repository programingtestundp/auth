import 'package:auth/extensions/context/index.dart';
import 'package:auth/view/screens/Auth_Screens/widgets/index.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class RegisterAppBar extends ConsumerWidget {
  const RegisterAppBar({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SizedBox(
      height: context.isPortrait ? 70.h : 90.h,
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: [
          Container(
            alignment: Alignment.center,
            width: 296.w,
            height: context.isPortrait ? 56.h : 66.h,
            child: Text(context.loc.weeky_coupos,
                style: context.styles.appBarTextStyle0),
          ),
          Container(
            alignment: Alignment.center,
            width: 300.w,
            height: context.isPortrait ? 60.h : 70.h,
            child: Text(context.loc.weeky_coupos,
                style:
                    context.styles.appBarTextStyle1.copyWith(fontSize: 32.sp)),
          ),
        ],
      ),
    );
  }
}
