import 'package:auth/extensions/context/index.dart';
import 'package:auth/view/screens/Auth_Screens/widgets/index.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../state/providers/ui providers/registration/registration_providers.dart';

class TopBar extends StatelessWidget {
  const TopBar({super.key});

  @override
  Widget build(BuildContext context) {
    return const Row(
      children: [
        Expanded(
          flex: 1,
          child: TopBarElement(index: 0),
        ),
        Expanded(
          flex: 1,
          child: TopBarElement(index: 1),
        ),
      ],
    );
  }
}

class TopBarElement extends ConsumerWidget {
  const TopBarElement({
    super.key,
    required this.index,
  });

  final int index;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final List<String> _barList = [context.loc.login, context.loc.signUp];
    final pageIndex = ref.watch(switchRegistrationPageProvider);
    return GestureDetector(
        onTap: () {
          ref.read(switchRegistrationPageProvider.notifier).setState = index;
        },
        child: Column(
          children: [
            Container(
              color: pageIndex == index
                  ? context.themes.thirdColor
                  : context.themes.color8,
              height: 2.h,
            ),
            Container(
              color: pageIndex == index
                  ? context.themes.backGroundColor1
                  : context.themes.color8,
              width: MediaQuery.of(context).size.width / 2,
              height: context.isPortrait ? 40.h : 60.h,
              child: Center(
                child: Text(
                  _barList[index],
                  style: context.styles.style1.copyWith(
                    color: pageIndex == index
                        ? context.themes.color7
                        : Colors.blueGrey,
                    fontWeight:
                        pageIndex == 0 ? FontWeight.bold : FontWeight.normal,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ));
  }
}
