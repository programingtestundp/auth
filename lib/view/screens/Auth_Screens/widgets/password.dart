import 'package:auth/extensions/context/index.dart';

import '../../../../state/auth/providers/password_prvider.dart';
import 'index.dart';

class PasswordWid extends ConsumerWidget {
  const PasswordWid({
    super.key,
  });
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Validations validations = Validations();
    return MyTextFormFieldWed(
      validator: validations.passwordValidations().call,
      iconPrefixIcon: Icon(
        Icons.password,
        size: 20.r,
        color: const Color(0xFF4c4f7a),
      ),
      hinText: context.loc.password,
      hintStyle: context.styles.style1.copyWith(color: Colors.grey),
      inputTextStyle:
          context.styles.style1.copyWith(color: context.themes.textColor),
      focusedBorderColor: context.themes.secondaryColor!,
      fillColor: context.themes.backGroundColor2!,
      activeIsObscure: true,
      borderRadius: 15.r,
      onChanged: (value) {
        ref.watch(passwordProvider.notifier).setState = value;
      }.call,
    );
  }
}
