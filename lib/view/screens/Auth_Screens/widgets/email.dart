import 'package:auth/extensions/context/index.dart';

import '../../../../state/auth/providers/dummy_user_provider.dart';
import '../../../../state/providers/ui providers/registration/initial_registration_proviser.dart';
import 'index.dart';

class EmailWid extends ConsumerWidget {
  const EmailWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Validations validations = Validations();
    final autoVal = ref.watch(autoValidateTextFieldProvider);

    return MyTextFormFieldWed(
      validator: validations.emailValidations().call,
      iconPrefixIcon: Icon(
        Icons.email,
        size: context.isPortrait ? 20.r : 30.r,
        color: const Color(0xFF4c4f7a),
      ),
      hinText: context.loc.email,
      hintStyle: context.styles.style1.copyWith(color: Colors.grey),
      inputTextStyle:
          context.styles.style1.copyWith(color: context.themes.textColor),
      focusedBorderColor: context.themes.secondaryColor!,
      fillColor: context.themes.backGroundColor2!,
      activeIsObscure: false,
      // isAuto: resetVal.value,
      borderRadius: 15.r,
      isAuto: autoVal,
      onChanged: (value) {
        ref.watch(userProvider.notifier).setUserData(email: value);
      },
    );
  }
}
