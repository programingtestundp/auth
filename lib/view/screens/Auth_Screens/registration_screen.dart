import 'package:auth/extensions/context/index.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../state/providers/ui providers/message_provider.dart';
import '../../../state/providers/ui providers/registration/initial_registration_proviser.dart';
import '../../../state/providers/ui providers/registration/registration_providers.dart';
import '../../../utils/ui/gen/assets.gen.dart';
import 'login_page.dart';
import 'signup_page.dart';
import 'widgets/Appbar.dart';
import 'widgets/top_bar.dart';

@RoutePage()
class RegistrationScreen extends ConsumerWidget {
  const RegistrationScreen({super.key, this.container});
  final ProviderContainer? container;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pageIndex = ref.watch(switchRegistrationPageProvider);
    // ▶ initial date to get the local country  ◀ //
    final initial = ref.watch(initialRegistrationProvider);
// ▶ to show error message ◀ //
    ref.listen(
      messageProvider,
      (previous, next) {
        if (next != '' && previous == '') {
          ref
              .read(messageProvider.notifier)
              .trigger(context, backgroundColor: Colors.red, duration: 5000);
        }
      },
    );

    return GestureDetector(
      key: const Key('RegistrationScreen-GestureDetector'),
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        backgroundColor: context.themes.primaryColor!.withOpacity(0.6),
        body: Center(
          child: Scaffold(
            appBar: AppBar(
              toolbarHeight: context.isPortrait ? 80.h : 130.h,
              flexibleSpace: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: Assets.images.header.provider(),
                        fit: BoxFit.fill,
                        colorFilter: ColorFilter.mode(
                            context.themes.opacityColor1!, BlendMode.srcATop))),
              ),
              title: const RegisterAppBar(),
            ),
            body: initial.when(
                data: (data) => Column(
                      key: const Key('RegistrationScreen-Column'),
                      children: [
                        const TopBar(),
                        Expanded(
                            child: pageIndex == 0
                                ? const LoginScreen()
                                : const SignUpScreen()),
                      ],
                    ),
                error: (error, stackTrace) => Center(
                      key: const Key('RegistrationScreen-error'),
                      child: Text(
                        'initial error:$error',
                        style:
                            context.styles.style1.copyWith(color: Colors.red),
                      ),
                    ),
                loading: () => const Center(
                      key: const Key('RegistrationScreen-loading'),
                      child: CircularProgressIndicator(),
                    )),
          ),
        ),
      ),
    );
  }
}
