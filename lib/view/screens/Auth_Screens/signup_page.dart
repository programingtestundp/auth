import 'package:auth/extensions/context/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../state/auth/models/auth_result.dart';
import '../../../state/auth/providers/auth_state_notifier.dart';
import '../../../state/auth/providers/dummy_user_provider.dart';
import '../../../state/auth/providers/password_prvider.dart';
import '../../../state/auth/providers/phone_validation_provider.dart';
import '../../../state/auth/validations/validations.dart';
import '../../../state/providers/ui providers/registration/initial_registration_proviser.dart';
import '../../widgets/LoadDoneButton.dart';
import '../../widgets/my_text_field.dart';
import 'package:intl_phone_field/countries.dart' show Country, countries;
import 'package:intl_phone_field/country_picker_dialog.dart'
    show CountryPickerDialog;
import 'package:intl_phone_field/intl_phone_field.dart' show IntlPhoneField;
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget, useState;
import 'package:intl/intl.dart' show DateFormat;

import 'widgets/email.dart';
import 'widgets/password.dart';

class SignUpScreen extends HookWidget {
  const SignUpScreen({
    super.key,
  });
  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>(); // Get the nearest form

    return Scaffold(
      body: SafeArea(
        child: Form(
          key: formKey,
          child: Center(
            child: SingleChildScrollView(
              child: SizedBox(
                width: 280.w,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 10.h,
                    ),
                    // Name field
                    const _NameWid(),
                    SizedBox(
                      height: 10.h,
                    ),
                    // Email field
                    const EmailWid(),

                    SizedBox(height: 8.h),
                    //phone number
                    const _PhoneWid(),
                    SizedBox(height: 8.h),
                    //country select
                    const _CountryWid(),
                    SizedBox(
                      height: 10.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        const _DateWid(),
                        SizedBox(
                          width: 10.h,
                        ),
                        const _GenderWid(),
                      ],
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    const PasswordWid(),

                    SizedBox(
                      height: 10.h,
                    ),
                    const _ConfirmPasswordWid(),
                    SizedBox(
                      height: 20.h,
                    ),
                    _SignUpWid(
                      formKey: formKey,
                    ),
                    SizedBox(
                      height: 30.h,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _NameWid extends ConsumerWidget {
  const _NameWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Validations validations = Validations();
    return MyTextFormFieldWed(
      iconPrefixIcon: const Icon(
        Icons.person,
        color: Color(0xFF4c4f7a),
      ),
      hinText: context.loc.enterYourName,
      hintStyle: context.styles.style1.copyWith(color: Colors.grey),
      inputTextStyle:
          context.styles.style1.copyWith(color: context.themes.textColor),
      focusedBorderColor: context.themes.secondaryColor!,
      fillColor: context.themes.backGroundColor2!,
      activeIsObscure: false,
      borderRadius: 15.r,
      validator: validations.nameValidations().call,
      onChanged: (value) {
        ref.watch(userProvider.notifier).setUserData(name: value);
      },
    );
  }
}

class _PhoneWid extends ConsumerWidget {
  const _PhoneWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final localRegionCode =
        ref.watch(userProvider.select((value) => value.phone.code));

    final validation = ref.watch(validatePhoneProvider);
    return IntlPhoneField(
      style: context.styles.style1.copyWith(color: Colors.black54),
      dropdownTextStyle: context.styles.style1.copyWith(color: Colors.black54),
      autovalidateMode: AutovalidateMode.disabled,
      disableLengthCheck: true,
      onSubmitted: (p0) => FocusManager.instance.primaryFocus?.unfocus(),
      languageCode: localRegionCode,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(
            horizontal: 10.w, vertical: context.isPortrait ? 20.h : 30.h),
        errorText: validation.isEmpty ? null : validation,
        labelText: context.loc.phoneNumber,
        labelStyle: context.styles.style1.copyWith(color: Colors.black54),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.r),
            borderSide: BorderSide(
                color:
                    validation.isEmpty ? const Color(0xFF000000) : Colors.red)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.r),
            borderSide: BorderSide(
                color:
                    validation.isEmpty ? const Color(0xFF000000) : Colors.red)),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1.5.w)),
      ),
      initialCountryCode: 'SY',
      onChanged: (value) {
        ref.read(userProvider.notifier).setUserData(number: value.number);
      },
      onCountryChanged: (country) {
        ref
            .read(userProvider.notifier)
            .setUserData(code: country.code, dialCode: country.dialCode);
      },
    );
  }
}

class _CountryWid extends HookConsumerWidget {
  const _CountryWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final selectedCountryP =
        ref.read(initialRegistrationValuesProvider).selectedCountry;

    final ValueNotifier<Country?> selectedCountry = useState(selectedCountryP);
    return InkWell(
      onTap: () async {
        await showDialog(
            context: context,
            useRootNavigator: false,
            builder: (context) => CountryPickerDialog(
                  searchText: context.loc.searchCountry,
                  languageCode: 'en',
                  countryList: countries,
                  onCountryChanged: (Country value) {
                    selectedCountry.value = value;
                    ref
                        .watch(userProvider.notifier)
                        .setUserData(country: value.name);
                  },
                  filteredCountries: countries,
                  selectedCountry: selectedCountry.value!,
                ));
      },
      child: Container(
        height: context.isPortrait ? 50.h : 70.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.r),
            border: Border.all(width: 1, color: Colors.black45)),
        alignment: Alignment.center,
        child: Row(children: [
          Expanded(
            flex: 10,
            child: Row(
              children: [
                SizedBox(
                  width: 10.w,
                ),
                Container(
                  width: 40.r,
                  height: 40.r,
                  decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                      ),
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage(
                          'assets/flags/${selectedCountry.value!.code.toLowerCase()}.png',
                          package: 'intl_phone_field',
                        ),
                        fit: BoxFit.fill,
                      )),
                ),
                SizedBox(
                  width: 10.w,
                ),
                Text(
                  selectedCountry.value!.name,
                  style: context.styles.style1.copyWith(color: Colors.black54),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Icon(
              Icons.arrow_drop_down_outlined,
              size: 30.r,
            ),
          )
        ]),
      ),
    );
  }
}

class _DateWid extends ConsumerWidget {
  const _DateWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final date = ref.watch(userProvider.select((value) => value.birthDay));
    final bool dateError = date == '';
    final String dateText =
        date == '0000/00//00' || date == '' ? context.loc.dateOfBirth : date;

    return InkWell(
      onTap: () async {
        final DateTime? picked = await showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(DateTime.now().year - 100),
            lastDate: DateTime(2101));
        final birthDay =
            ref.read(userProvider.select((value) => value.birthDay));
        String? newDate;
        if (picked != null) {
          String pickedStr = DateFormat('yyyy/MM/dd').format(picked);
          if (pickedStr != birthDay) {
            newDate = pickedStr;
          }
        }
        ref.read(userProvider.notifier).setUserData(birthDay: newDate);
      },
      child: Container(
        width: 100.w,
        height: context.isPortrait ? 30.h : 50.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.w),
            border: Border.all(
                width: (dateError ? 1 : 0.5).w,
                color: dateError ? Colors.red : const Color(0xFF000000)),
            color: Colors.black12),
        alignment: Alignment.center,
        child: Text(
          dateText,
          style: context.styles.style1
              .copyWith(color: dateError ? Colors.red : Colors.black54),
        ),
      ),
    );
  }
}

class _GenderWid extends ConsumerWidget {
  const _GenderWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    String selectedGender =
        ref.watch(userProvider.select((value) => value.gender));
    selectedGender = context.loc.gender(selectedGender);
    return Container(
      width: 100.w,
      height: context.isPortrait ? 35.h : 50.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.w),
        border: Border.all(
          width: 1.w,
        ),
      ),
      alignment: Alignment.center,
      child: PopupMenuButton<String>(
        icon: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              selectedGender,
              style: context.styles.style1.copyWith(color: Colors.black54),
            ),
            Icon(
              Icons.arrow_drop_down_sharp,
              size: context.isPortrait ? 25.r : 35.r,
            )
          ],
        ),
        iconSize: 65.r,
        onSelected: (String value) {
          ref.read(userProvider.notifier).setUserData(gender: value);
        },
        itemBuilder: (BuildContext context) {
          return <PopupMenuEntry<String>>[
            PopupMenuItem<String>(
              value: context.loc.male,
              child: Text(
                context.loc.male,
                style: context.styles.style1.copyWith(color: Colors.black54),
              ),
            ),
            PopupMenuItem<String>(
              value: context.loc.female,
              child: Text(context.loc.female,
                  style: context.styles.style1.copyWith(color: Colors.black54)),
            ),
          ];
        },
      ),
    );
  }
}

class _ConfirmPasswordWid extends ConsumerWidget {
  const _ConfirmPasswordWid({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Validations validations = Validations();
    return MyTextFormFieldWed(
      // focusNode: focusNode,
      iconPrefixIcon: Icon(
        Icons.password,
        size: 20.r,
        color: const Color(0xFF4c4f7a),
      ),
      hinText: context.loc.confirmYourPassword,
      hintStyle: context.styles.style1.copyWith(color: Colors.grey),
      inputTextStyle:
          context.styles.style1.copyWith(color: context.themes.textColor),
      focusedBorderColor: context.themes.secondaryColor!,
      fillColor: context.themes.backGroundColor2!,
      activeIsObscure: true,

      isAuto: false,
      borderRadius: 15.r,
      onChanged: (value) {
        ref.watch(passwordProvider.notifier).confirm = value;
      },
      validator: validations
          .passwordValidations(confirmPassword: ref.watch(passwordProvider))
          .call,
    );
  }
}

class _SignUpWid extends ConsumerWidget {
  const _SignUpWid({
    super.key,
    required this.formKey,
  });
  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return LoadDoneButton(
      text: context.loc.signUp,
      width: 150.w,
      hight: context.isPortrait ? 40.h : 50.h,
      fontSize: 10.r,
      navigationPath: '/home',
      color: context.themes.thirdColor!,
      function: (ref) async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();
          return ref.read(authNotifierProvider.notifier).signUp();
        }
        ref.read(autoValidateTextFieldProvider.notifier).setState = true;
        return AuthResults.failure;
      },
    );
  }
}
