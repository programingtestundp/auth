import 'package:flutter/material.dart';

List<BoxShadow> shadowList(
    {double spreadRadius = 0.05,
    double blurRadius = 2,
    Color color = Colors.black,
    Offset? offset}) {
  return [
    BoxShadow(
        offset: offset ?? Offset(2.5, 1.5),
        color: color,
        spreadRadius: spreadRadius,
        blurRadius: blurRadius),
  ];
}
