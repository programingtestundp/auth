/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vector_graphics/vector_graphics.dart';

class $AssetsIconsGen {
  const $AssetsIconsGen();

  /// File path: assets/icons/circle-heat-svgrepo-com.svg
  SvgGenImage get circleHeatSvgrepoCom =>
      const SvgGenImage('assets/icons/circle-heat-svgrepo-com.svg');

  /// List of all assets
  List<SvgGenImage> get values => [circleHeatSvgrepoCom];
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/Gg.png
  AssetGenImage get gg => const AssetGenImage('assets/images/Gg.png');

  /// File path: assets/images/GoldStar.png
  AssetGenImage get goldStar =>
      const AssetGenImage('assets/images/GoldStar.png');

  /// File path: assets/images/IMG_20211204_100310_719.jpg
  AssetGenImage get img20211204100310719 =>
      const AssetGenImage('assets/images/IMG_20211204_100310_719.jpg');

  /// File path: assets/images/UserIm.png
  AssetGenImage get userIm => const AssetGenImage('assets/images/UserIm.png');

  /// File path: assets/images/UserImlarge.png
  AssetGenImage get userImlarge =>
      const AssetGenImage('assets/images/UserImlarge.png');

  /// File path: assets/images/WCLOGO.png
  AssetGenImage get wclogo => const AssetGenImage('assets/images/WCLOGO.png');

  /// File path: assets/images/all_colors.jpg
  AssetGenImage get allColors =>
      const AssetGenImage('assets/images/all_colors.jpg');

  /// File path: assets/images/b1.jpg
  AssetGenImage get b1 => const AssetGenImage('assets/images/b1.jpg');

  /// File path: assets/images/b3.jpg
  AssetGenImage get b3 => const AssetGenImage('assets/images/b3.jpg');

  /// File path: assets/images/b6.jpg
  AssetGenImage get b6 => const AssetGenImage('assets/images/b6.jpg');

  /// File path: assets/images/bell0.gif
  AssetGenImage get bell0Gif => const AssetGenImage('assets/images/bell0.gif');

  /// File path: assets/images/bell0.png
  AssetGenImage get bell0Png => const AssetGenImage('assets/images/bell0.png');

  /// File path: assets/images/bell1.gif
  AssetGenImage get bell1 => const AssetGenImage('assets/images/bell1.gif');

  /// File path: assets/images/black1.jpg
  AssetGenImage get black1 => const AssetGenImage('assets/images/black1.jpg');

  /// File path: assets/images/black2.jpg
  AssetGenImage get black2 => const AssetGenImage('assets/images/black2.jpg');

  /// File path: assets/images/blue1.jpg
  AssetGenImage get blue1 => const AssetGenImage('assets/images/blue1.jpg');

  /// File path: assets/images/camping-on-top-of-the-mountain-during-sunset-free-photo.webp
  AssetGenImage get campingOnTopOfTheMountainDuringSunsetFreePhoto =>
      const AssetGenImage(
          'assets/images/camping-on-top-of-the-mountain-during-sunset-free-photo.webp');

  /// File path: assets/images/cars.jpeg
  AssetGenImage get cars => const AssetGenImage('assets/images/cars.jpeg');

  /// File path: assets/images/clipart.png
  AssetGenImage get clipart => const AssetGenImage('assets/images/clipart.png');

  /// File path: assets/images/confused.png
  AssetGenImage get confused =>
      const AssetGenImage('assets/images/confused.png');

  /// File path: assets/images/contactUs.png
  AssetGenImage get contactUs =>
      const AssetGenImage('assets/images/contactUs.png');

  /// File path: assets/images/coponsImg1.jpg
  AssetGenImage get coponsImg1 =>
      const AssetGenImage('assets/images/coponsImg1.jpg');

  /// File path: assets/images/draw.jpg
  AssetGenImage get draw => const AssetGenImage('assets/images/draw.jpg');

  /// File path: assets/images/eyes.webp
  AssetGenImage get eyes => const AssetGenImage('assets/images/eyes.webp');

  /// File path: assets/images/eyes2.gif
  AssetGenImage get eyes2 => const AssetGenImage('assets/images/eyes2.gif');

  /// File path: assets/images/food.jpg
  AssetGenImage get food => const AssetGenImage('assets/images/food.jpg');

  /// File path: assets/images/games.jpg
  AssetGenImage get games => const AssetGenImage('assets/images/games.jpg');

  /// File path: assets/images/header.jpg
  AssetGenImage get header => const AssetGenImage('assets/images/header.jpg');

  /// File path: assets/images/images.png
  AssetGenImage get images => const AssetGenImage('assets/images/images.png');

  /// File path: assets/images/laugh.png
  AssetGenImage get laugh => const AssetGenImage('assets/images/laugh.png');

  /// File path: assets/images/middle.png
  AssetGenImage get middle => const AssetGenImage('assets/images/middle.png');

  /// File path: assets/images/new-red-white-text-sticker.png
  AssetGenImage get newRedWhiteTextSticker =>
      const AssetGenImage('assets/images/new-red-white-text-sticker.png');

  /// File path: assets/images/pngwing.png
  AssetGenImage get pngwing => const AssetGenImage('assets/images/pngwing.png');

  /// File path: assets/images/pv.jpg
  AssetGenImage get pv => const AssetGenImage('assets/images/pv.jpg');

  /// File path: assets/images/register.png
  AssetGenImage get register =>
      const AssetGenImage('assets/images/register.png');

  /// File path: assets/images/sad.png
  AssetGenImage get sad => const AssetGenImage('assets/images/sad.png');

  /// File path: assets/images/senNoti.png
  AssetGenImage get senNoti => const AssetGenImage('assets/images/senNoti.png');

  /// File path: assets/images/smile.png
  AssetGenImage get smile => const AssetGenImage('assets/images/smile.png');

  /// File path: assets/images/smiley.png
  AssetGenImage get smiley => const AssetGenImage('assets/images/smiley.png');

  /// File path: assets/images/strange.png
  AssetGenImage get strange => const AssetGenImage('assets/images/strange.png');

  /// File path: assets/images/themeicon1.jpg
  AssetGenImage get themeicon1 =>
      const AssetGenImage('assets/images/themeicon1.jpg');

  /// File path: assets/images/wp3246103.jpg
  AssetGenImage get wp3246103 =>
      const AssetGenImage('assets/images/wp3246103.jpg');

  /// List of all assets
  List<AssetGenImage> get values => [
        gg,
        goldStar,
        img20211204100310719,
        userIm,
        userImlarge,
        wclogo,
        allColors,
        b1,
        b3,
        b6,
        bell0Gif,
        bell0Png,
        bell1,
        black1,
        black2,
        blue1,
        campingOnTopOfTheMountainDuringSunsetFreePhoto,
        cars,
        clipart,
        confused,
        contactUs,
        coponsImg1,
        draw,
        eyes,
        eyes2,
        food,
        games,
        header,
        images,
        laugh,
        middle,
        newRedWhiteTextSticker,
        pngwing,
        pv,
        register,
        sad,
        senNoti,
        smile,
        smiley,
        strange,
        themeicon1,
        wp3246103
      ];
}

class Assets {
  Assets._();

  static const $AssetsIconsGen icons = $AssetsIconsGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName, {this.size = null});

  final String _assetName;

  final Size? size;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(
    this._assetName, {
    this.size = null,
  }) : _isVecFormat = false;

  const SvgGenImage.vec(
    this._assetName, {
    this.size = null,
  }) : _isVecFormat = true;

  final String _assetName;

  final Size? size;
  final bool _isVecFormat;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme? theme,
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture(
      _isVecFormat
          ? AssetBytesLoader(_assetName,
              assetBundle: bundle, packageName: package)
          : SvgAssetLoader(_assetName,
              assetBundle: bundle, packageName: package),
      key: key,
      matchTextDirection: matchTextDirection,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter ??
          (color == null ? null : ColorFilter.mode(color, colorBlendMode)),
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
