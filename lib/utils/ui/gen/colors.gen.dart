/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/painting.dart';
import 'package:flutter/material.dart';

class ColorName {
  ColorName._();

  /// Color: #000000
  static const Color black = Color(0xFF000000);

  /// Color: #2464cc
  static const Color blue = Color(0xFF2464CC);

  /// Color: #a52a2a
  static const Color brown = Color(0xFFA52A2A);

  /// Color: #6495ED
  static const Color cornflowerBlue = Color(0xFF6495ED);

  /// Color: #DC143C
  static const Color crimson = Color(0xFFDC143C);

  /// Color: #00ffff
  static const Color cyan = Color(0xFF00FFFF);

  /// Color: #FF8F00
  static const Color darkAmber = Color(0xFFFF8F00);

  /// Color: #00008B
  static const Color darkBlue = Color(0xFF00008B);

  /// Color: #8B4513
  static const Color darkBrown = Color(0xFF8B4513);

  /// Color: #008B8B
  static const Color darkCyan = Color(0xFF008B8B);

  /// Color: #E64A19
  static const Color darkDeepOrange = Color(0xFFE64A19);

  /// Color: #E91E63
  static const Color darkDeepPink = Color(0xFFE91E63);

  /// Color: #512DA8
  static const Color darkDeepPurple = Color(0xFF512DA8);

  /// Color: #B8860B
  static const Color darkGoldenrod = Color(0xFFB8860B);

  /// Color: #A9A9A9
  static const Color darkGray = Color(0xFFA9A9A9);

  /// Color: #006400
  static const Color darkGreen = Color(0xFF006400);

  /// Color: #646c8c
  static const Color darkGrey = Color(0xFF646C8C);

  /// Color: #303F9F
  static const Color darkIndigo = Color(0xFF303F9F);

  /// Color: #BDB76B
  static const Color darkKhaki = Color(0xFFBDB76B);

  /// Color: #B39DDB
  static const Color darkLavender = Color(0xFFB39DDB);

  /// Color: #0288D1
  static const Color darkLightBlue = Color(0xFF0288D1);

  /// Color: #32CD32
  static const Color darkLime = Color(0xFF32CD32);

  /// Color: #8B008B
  static const Color darkMagenta = Color(0xFF8B008B);

  /// Color: #556B2F
  static const Color darkOlive = Color(0xFF556B2F);

  /// Color: #FF8C00
  static const Color darkOrange = Color(0xFFFF8C00);

  /// Color: #9932CC
  static const Color darkOrchid = Color(0xFF9932CC);

  /// Color: #FF1493
  static const Color darkPink = Color(0xFFFF1493);

  /// Color: #4B0082
  static const Color darkPurple = Color(0xFF4B0082);

  /// Color: #8B0000
  static const Color darkRed = Color(0xFF8B0000);

  /// Color: #E9967A
  static const Color darkSalmon = Color(0xFFE9967A);

  /// Color: #8FBC8F
  static const Color darkSeaGreen = Color(0xFF8FBC8F);

  /// Color: #483D8B
  static const Color darkSlateBlue = Color(0xFF483D8B);

  /// Color: #2F4F4F
  static const Color darkSlateGray = Color(0xFF2F4F4F);

  /// Color: #008B8B
  static const Color darkTeal = Color(0xFF008B8B);

  /// Color: #00CED1
  static const Color darkTurquoise = Color(0xFF00CED1);

  /// Color: #9400D3
  static const Color darkViolet = Color(0xFF9400D3);

  /// Color: #BDB76B
  static const Color darkYellow = Color(0xFFBDB76B);

  /// Color: #FF1493
  static const Color deepPink = Color(0xFFFF1493);

  /// Color: #00BFFF
  static const Color deepSkyBlue = Color(0xFF00BFFF);

  /// Color: #B22222
  static const Color firebrick = Color(0xFFB22222);

  /// Color: #DAA520
  static const Color goldenrod = Color(0xFFDAA520);

  /// Color: #808080
  static const Color gray = Color(0xFF808080);

  /// Color: #00ff00
  static const Color green = Color(0xFF00FF00);

  /// Color: #ADFF2F
  static const Color greenYellow = Color(0xFFADFF2F);

  /// Color: #FF69B4
  static const Color hotPink = Color(0xFFFF69B4);

  /// Color: #CD5C5C
  static const Color indianRed = Color(0xFFCD5C5C);

  /// Color: #FFC107
  static const Color lightAmber = Color(0xFFFFC107);

  /// Color: #ADD8E6
  static const Color lightBlue = Color(0xFFADD8E6);

  /// Color: #D2691E
  static const Color lightBrown = Color(0xFFD2691E);

  /// Color: #E0FFFF
  static const Color lightCyan = Color(0xFFE0FFFF);

  /// Color: #FF5722
  static const Color lightDeepOrange = Color(0xFFFF5722);

  /// Color: #FF4081
  static const Color lightDeepPink = Color(0xFFFF4081);

  /// Color: #673AB7
  static const Color lightDeepPurple = Color(0xFF673AB7);

  /// Color: #D3D3D3
  static const Color lightGray = Color(0xFFD3D3D3);

  /// Color: #90EE90
  static const Color lightGreen = Color(0xFF90EE90);

  /// Color: #9494ac
  static const Color lightGrey = Color(0xFF9494AC);

  /// Color: #3F51B5
  static const Color lightIndigo = Color(0xFF3F51B5);

  /// Color: #E6E6FA
  static const Color lightLavender = Color(0xFFE6E6FA);

  /// Color: #03A9F4
  static const Color lightLightBlue = Color(0xFF03A9F4);

  /// Color: #00FF00
  static const Color lightLime = Color(0xFF00FF00);

  /// Color: #808000
  static const Color lightOlive = Color(0xFF808000);

  /// Color: #FFB6C1
  static const Color lightPink = Color(0xFFFFB6C1);

  /// Color: #BA55D3
  static const Color lightPurple = Color(0xFFBA55D3);

  /// Color: #FFC0CB
  static const Color lightRed = Color(0xFFFFC0CB);

  /// Color: #FFA07A
  static const Color lightSalmon = Color(0xFFFFA07A);

  /// Color: #20B2AA
  static const Color lightSeaGreen = Color(0xFF20B2AA);

  /// Color: #778899
  static const Color lightSlateGray = Color(0xFF778899);

  /// Color: #B0C4DE
  static const Color lightSteelBlue = Color(0xFFB0C4DE);

  /// Color: #40E0D0
  static const Color lightTeal = Color(0xFF40E0D0);

  /// Color: #FFFFE0
  static const Color lightYellow = Color(0xFFFFFFE0);

  /// Color: #32CD32
  static const Color limeGreen = Color(0xFF32CD32);

  /// Color: #800000
  static const Color maroon = Color(0xFF800000);

  /// Color: #66CDAA
  static const Color mediumAquamarine = Color(0xFF66CDAA);

  /// Color: #0000CD
  static const Color mediumBlue = Color(0xFF0000CD);

  /// Color: #666666
  static const Color mediumGray = Color(0xFF666666);

  /// Color: #BA55D3
  static const Color mediumOrchid = Color(0xFFBA55D3);

  /// Color: #9370DB
  static const Color mediumPurple = Color(0xFF9370DB);

  /// Color: #3CB371
  static const Color mediumSeaGreen = Color(0xFF3CB371);

  /// Color: #7B68EE
  static const Color mediumSlateBlue = Color(0xFF7B68EE);

  /// Color: #00FA9A
  static const Color mediumSpringGreen = Color(0xFF00FA9A);

  /// Color: #48D1CC
  static const Color mediumTurquoise = Color(0xFF48D1CC);

  /// Color: #C71585
  static const Color mediumVioletRed = Color(0xFFC71585);

  /// Color: #6B8E23
  static const Color oliveDrab = Color(0xFF6B8E23);

  /// Color: #ff7f00
  static const Color orange = Color(0xFFFF7F00);

  /// Color: #FF4500
  static const Color orangered = Color(0xFFFF4500);

  /// Color: #EEE8AA
  static const Color paleGoldenrod = Color(0xFFEEE8AA);

  /// Color: #98FB98
  static const Color paleGreen = Color(0xFF98FB98);

  /// Color: #DB7093
  static const Color paleVioletRed = Color(0xFFDB7093);

  /// Color: #ff69b4
  static const Color pink = Color(0xFFFF69B4);

  /// MaterialColor:
  ///   50: #FFE5E6EB
  ///   100: #FFBEBFCE
  ///   200: #FF9295AE
  ///   300: #FF666B8D
  ///   400: #FF464B74
  ///   500: #FF252B5C
  ///   600: #FF212654
  ///   700: #FF1B204A
  ///   800: #FF161A41
  ///   900: #FF0D1030
  static const MaterialColor primarySwatch = MaterialColor(
    0xFF252B5C,
    <int, Color>{
      50: Color(0xFFE5E6EB),
      100: Color(0xFFBEBFCE),
      200: Color(0xFF9295AE),
      300: Color(0xFF666B8D),
      400: Color(0xFF464B74),
      500: Color(0xFF252B5C),
      600: Color(0xFF212654),
      700: Color(0xFF1B204A),
      800: Color(0xFF161A41),
      900: Color(0xFF0D1030),
    },
  );

  /// Color: #800080
  static const Color purple = Color(0xFF800080);

  /// Color: #ff0000
  static const Color red = Color(0xFFFF0000);

  /// Color: #BC8F8F
  static const Color rosyBrown = Color(0xFFBC8F8F);

  /// Color: #4169E1
  static const Color royalBlue = Color(0xFF4169E1);

  /// Color: #8B4513
  static const Color saddleBrown = Color(0xFF8B4513);

  /// Color: #A0522D
  static const Color sienna = Color(0xFFA0522D);

  /// Color: #708090
  static const Color slateGray = Color(0xFF708090);

  /// Color: #00FF7F
  static const Color springGreen = Color(0xFF00FF7F);

  /// Color: #008080
  static const Color teal = Color(0xFF008080);

  /// Color: #ffffff
  static const Color white = Color(0xFFFFFFFF);

  /// Color: #fcd45d
  static const Color yellow = Color(0xFFFCD45D);
}
