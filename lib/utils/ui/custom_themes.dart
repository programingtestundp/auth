import 'package:flutter/material.dart';

@immutable
class CustomColors extends ThemeExtension<CustomColors> {
  const CustomColors({
    required this.backGroundColor1,
    required this.backGroundColor2,
    required this.primaryColor,
    required this.secondaryColor,
    required this.thirdColor,
    required this.textColor,
    required this.textColor2,
    required this.linkColor,
    required this.color1,
    required this.color2,
    required this.color3,
    required this.color4,
    required this.color5,
    required this.color6,
    required this.color7,
    required this.color8,
    required this.opacityColor1,
    required this.opacityColor2,
    required this.opacityColor3,
    required this.opacityColor4,
    required this.gColor1,
    required this.gColor2,
    required this.gColor3,
    required this.gColor4,
    required this.gColor5,
    required this.gColor6,
    required this.gColor7,
    required this.gColor8,
    required this.gColor9,
    required this.gColor10,
    required this.gColor11,
    required this.gColor12,
    required this.gColor13,
  });
  final Color? backGroundColor1;
  final Color? backGroundColor2;
  final Color? primaryColor;
  final Color? secondaryColor;
  final Color? thirdColor;
  final Color? textColor;
  final Color? textColor2;
  final Color? linkColor;
  final Color? opacityColor1;
  final Color? opacityColor2;
  final Color? opacityColor3;
  final Color? opacityColor4;
  final Color? color1;
  final Color? color2;
  final Color? color3;
  final Color? color4;
  final Color? color5;
  final Color? color6;
  final Color? color7;
  final Color? color8;
  final Color? gColor1;
  final Color? gColor2;
  final Color? gColor3;
  final Color? gColor4;
  final Color? gColor5;
  final Color? gColor6;
  final Color? gColor7;
  final Color? gColor8;
  final Color? gColor9;
  final Color? gColor10;
  final Color? gColor11;
  final Color? gColor12;
  final Color? gColor13;

  @override
  CustomColors copyWith({
    Color? backGroundColor1,
    Color? backGroundColor2,
    Color? primaryColor,
    Color? secondaryColor,
    Color? thirdColor,
    Color? textColor,
    Color? textColor2,
    Color? linkColor,
    Color? opacityColor1,
    Color? opacityColor2,
    Color? opacityColor3,
    Color? opacityColor4,
    Color? color1,
    Color? color2,
    Color? color3,
    Color? color4,
    Color? color5,
    Color? color6,
    Color? color7,
    Color? color8,
    Color? gColor1,
    Color? gColor2,
    Color? gColor3,
    Color? gColor4,
    Color? gColor5,
    Color? gColor6,
    Color? gColor7,
    Color? gColor8,
    Color? gColor9,
    Color? gColor10,
    Color? gColor11,
    Color? gColor12,
    Color? gColor13,
  }) {
    return CustomColors(
      backGroundColor1: backGroundColor1 ?? this.backGroundColor1,
      backGroundColor2: backGroundColor2 ?? this.backGroundColor2,
      primaryColor: primaryColor ?? this.primaryColor,
      secondaryColor: secondaryColor ?? this.secondaryColor,
      thirdColor: thirdColor ?? this.thirdColor,
      textColor: textColor ?? this.textColor,
      textColor2: textColor2 ?? this.textColor2,
      linkColor: linkColor ?? this.linkColor,
      opacityColor1: opacityColor1 ?? this.opacityColor1,
      opacityColor2: opacityColor2 ?? this.opacityColor2,
      opacityColor3: opacityColor3 ?? this.opacityColor3,
      opacityColor4: opacityColor4 ?? this.opacityColor4,
      color1: color1 ?? this.color1,
      color2: color2 ?? this.color2,
      color3: color3 ?? this.color3,
      color4: color4 ?? this.color4,
      color5: color5 ?? this.color5,
      color6: color6 ?? this.color6,
      color7: color7 ?? this.color7,
      color8: color8 ?? this.color8,
      gColor1: gColor1 ?? this.gColor1,
      gColor2: gColor2 ?? this.gColor2,
      gColor3: gColor3 ?? this.gColor3,
      gColor4: gColor4 ?? this.gColor4,
      gColor5: gColor5 ?? this.gColor5,
      gColor6: gColor6 ?? this.gColor6,
      gColor7: gColor7 ?? this.gColor7,
      gColor8: gColor8 ?? this.gColor8,
      gColor9: gColor9 ?? this.gColor9,
      gColor10: gColor10 ?? this.gColor10,
      gColor11: gColor11 ?? this.gColor11,
      gColor12: gColor12 ?? this.gColor12,
      gColor13: gColor13 ?? this.gColor13,
    );
  }

  // Controls how the properties change on theme changes
  @override
  CustomColors lerp(ThemeExtension<CustomColors>? other, double t) {
    if (other is! CustomColors) {
      return this;
    }
    return CustomColors(
      backGroundColor1: Color.lerp(backGroundColor1, other.backGroundColor1, t),
      backGroundColor2: Color.lerp(backGroundColor2, other.backGroundColor2, t),
      primaryColor: Color.lerp(primaryColor, other.primaryColor, t),
      secondaryColor: Color.lerp(secondaryColor, other.secondaryColor, t),
      thirdColor: Color.lerp(thirdColor, other.thirdColor, t),
      textColor: Color.lerp(textColor, other.textColor, t),
      textColor2: Color.lerp(textColor2, other.textColor2, t),
      linkColor: Color.lerp(linkColor, other.linkColor, t),
      opacityColor1: Color.lerp(opacityColor1, other.opacityColor1, t),
      opacityColor2: Color.lerp(opacityColor2, other.opacityColor2, t),
      opacityColor3: Color.lerp(opacityColor3, other.opacityColor3, t),
      opacityColor4: Color.lerp(opacityColor4, other.opacityColor4, t),
      color1: Color.lerp(color1, other.color1, t),
      color2: Color.lerp(color2, other.color2, t),
      color3: Color.lerp(color3, other.color3, t),
      color4: Color.lerp(color4, other.color4, t),
      color5: Color.lerp(color5, other.color5, t),
      color6: Color.lerp(color6, other.color6, t),
      color7: Color.lerp(color7, other.color7, t),
      color8: Color.lerp(color8, other.color8, t),
      gColor1: Color.lerp(gColor1, other.gColor1, t),
      gColor2: Color.lerp(gColor2, other.gColor2, t),
      gColor3: Color.lerp(gColor3, other.gColor3, t),
      gColor4: Color.lerp(gColor4, other.gColor4, t),
      gColor5: Color.lerp(gColor5, other.gColor5, t),
      gColor6: Color.lerp(gColor6, other.gColor6, t),
      gColor7: Color.lerp(gColor7, other.gColor7, t),
      gColor8: Color.lerp(gColor8, other.gColor8, t),
      gColor9: Color.lerp(gColor9, other.gColor9, t),
      gColor10: Color.lerp(gColor10, other.gColor10, t),
      gColor11: Color.lerp(gColor11, other.gColor11, t),
      gColor12: Color.lerp(gColor12, other.gColor12, t),
      gColor13: Color.lerp(gColor13, other.gColor13, t),
    );
  }

  // the dark theme
  static CustomColors blue = CustomColors(
    backGroundColor1: Colors.white,
    backGroundColor2: Colors.white,
    primaryColor: Colors.lightBlue,
    secondaryColor: Colors.lightBlue,
    thirdColor: Colors.lightBlue,
    textColor: Colors.black.withOpacity(0.7),
    textColor2: Colors.black.withOpacity(0.7),
    linkColor: Colors.blue,
    color1: Colors.white,
    color2: Color.fromARGB(255, 223, 217, 217),
    color3: Colors.white,
    color4: Colors.red,
    color5: Color.fromARGB(255, 92, 92, 92),
    color6: Color.fromARGB(255, 231, 231, 231),
    color7: Colors.blue,
    color8: Colors.black12,
    opacityColor1: Colors.white.withOpacity(0.3),
    opacityColor2: Colors.black.withOpacity(0.6),
    opacityColor3: Colors.transparent,
    opacityColor4: Colors.transparent,
    gColor1: Colors.blue,
    gColor2: Colors.white,
    gColor3: Colors.blue,
    gColor4: Color.fromARGB(255, 73, 172, 253),
    gColor5: Color.fromARGB(255, 119, 192, 252),
    gColor6: Colors.white.withOpacity(0.2),
    gColor7: Colors.white.withOpacity(0.6),
    gColor8: Colors.white.withOpacity(0.0),
    gColor9: Colors.white.withOpacity(0.6),
    gColor10: Colors.white.withOpacity(0.2),
    gColor11: Color.fromARGB(255, 5, 233, 221),
    gColor12: Colors.grey[200]!,
    gColor13: Color.fromARGB(255, 54, 66, 175),
  );

  static CustomColors black = CustomColors(
    backGroundColor1: Color.fromARGB(255, 5, 23, 46),
    backGroundColor2: Color.fromARGB(255, 51, 61, 65),
    primaryColor: Color.fromARGB(255, 5, 23, 46),
    secondaryColor: Colors.grey[200]!,
    thirdColor: Color.fromARGB(255, 51, 61, 65),
    textColor: Colors.grey[200]!,
    textColor2: Colors.grey,
    linkColor: Colors.lightBlue,
    color1: Colors.grey,
    color2: Colors.grey,
    color3: Colors.grey[200]!,
    color4: Colors.grey[200]!,
    color5: Colors.grey[200]!,
    color6: Color.fromARGB(255, 51, 61, 65),
    color7: Colors.grey[200]!,
    color8: Colors.white12,
    opacityColor1: Colors.black.withOpacity(0.5),
    opacityColor2: Colors.white.withOpacity(0.6),
    opacityColor3: Colors.black.withOpacity(0.3),
    opacityColor4: Colors.black.withOpacity(0.3),
    gColor1: Color.fromARGB(255, 5, 23, 46),
    gColor2: Color.fromARGB(255, 51, 61, 65),
    gColor3: Color.fromARGB(255, 5, 23, 46),
    gColor4: Color.fromARGB(255, 17, 28, 44),
    gColor5: Color.fromARGB(255, 72, 76, 82),
    gColor6: Colors.white.withOpacity(0.2),
    gColor7: Colors.white.withOpacity(0.4),
    gColor8: Colors.white.withOpacity(0.0),
    gColor9: Colors.white.withOpacity(0.6),
    gColor10: Colors.white.withOpacity(0.2),
    gColor11: Colors.grey[200]!,
    gColor12: Color.fromARGB(255, 103, 113, 117),
    gColor13: Color.fromARGB(255, 51, 61, 65),
  );

  static CustomColors green = CustomColors(
    backGroundColor1: Colors.white,
    backGroundColor2: Colors.white,
    primaryColor: Colors.green,
    secondaryColor: Colors.green,
    thirdColor: Colors.green,
    textColor: Colors.black.withOpacity(0.7),
    textColor2: Colors.black.withOpacity(0.7),
    linkColor: Colors.blue,
    color1: Colors.white,
    color2: Color.fromARGB(255, 223, 217, 217),
    color3: Colors.white,
    color4: Colors.red,
    color5: Color.fromARGB(255, 92, 92, 92),
    color6: Color.fromARGB(255, 231, 231, 231),
    color7: Colors.green,
    color8: Colors.black12,
    opacityColor1: Colors.white.withOpacity(0.3),
    opacityColor2: Colors.black.withOpacity(0.6),
    opacityColor3: Colors.transparent,
    opacityColor4: Colors.transparent,
    gColor1: Colors.green,
    gColor2: Colors.white,
    gColor3: Colors.green,
    gColor4: Color.fromARGB(255, 145, 253, 149),
    gColor5: Color.fromARGB(255, 183, 252, 186),
    gColor6: Colors.white.withOpacity(0.2),
    gColor7: Colors.white.withOpacity(0.6),
    gColor8: Colors.white.withOpacity(0.0),
    gColor9: Colors.white.withOpacity(0.6),
    gColor10: Colors.white.withOpacity(0.2),
    gColor11: Color.fromARGB(255, 5, 233, 221),
    gColor12: Colors.grey[200]!,
    gColor13: Color.fromARGB(255, 6, 243, 46),
  );

  static CustomColors purple = CustomColors(
      backGroundColor1: Color.fromARGB(255, 53, 1, 37),
      backGroundColor2: Color.fromARGB(255, 100, 1, 70),
      primaryColor: Color.fromARGB(255, 53, 1, 37), //background1
      secondaryColor: Colors.grey[200]!,
      thirdColor: Color.fromARGB(255, 100, 1, 70), //background2
      textColor: Colors.grey[200]!,
      textColor2: Colors.grey,
      linkColor: Colors.lightBlue,
      color1: Colors.grey,
      color2: Colors.grey,
      color3: Colors.grey[200]!,
      color4: Colors.grey[200]!,
      color5: Colors.grey[200]!,
      color6: Color.fromARGB(255, 100, 1, 70),
      color7: Colors.grey[200]!,
      color8: Colors.white12,
      opacityColor1: Color.fromARGB(255, 100, 1, 70).withOpacity(0.6),
      opacityColor2: Colors.white.withOpacity(0.6),
      opacityColor3: Color.fromARGB(255, 100, 1, 70).withOpacity(0.7),
      opacityColor4: Colors.black.withOpacity(0.3),
      gColor1: Color.fromARGB(255, 53, 1, 37),
      gColor2: Color.fromARGB(255, 100, 1, 70),
      gColor3: Color.fromARGB(255, 53, 1, 37),
      gColor4: Color.fromARGB(255, 121, 2, 85),
      gColor5: Color.fromARGB(255, 185, 1, 130),
      gColor6: Colors.white.withOpacity(0.2),
      gColor7: Colors.white.withOpacity(0.4),
      gColor8: Colors.white.withOpacity(0.0),
      gColor9: Colors.white.withOpacity(0.6),
      gColor10: Colors.white.withOpacity(0.2),
      gColor11: Colors.grey[200]!,
      gColor12: Color.fromARGB(255, 153, 69, 128),
      gColor13: Color.fromARGB(255, 100, 1, 70));
}
