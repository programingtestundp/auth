// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:auth/extensions/context/index.dart';
import 'package:auth/view/screens/Auth_Screens/widgets/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

final class CustomTextStyles {
  CustomTextStyles(BuildContext context)
      : appBarTextStyle0 = TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 30.sp,
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = 3
              ..color = context.themes.thirdColor!);
  final TextStyle style1 =
      TextStyle(fontFamily: 'Lora', fontSize: 10.sp, color: Colors.white);
  final TextStyle appBarTextStyle0;
  final TextStyle appBarTextStyle1 = TextStyle(
      fontFamily: 'Lora',
      fontSize: 20.sp,
      color: Colors.white,
      fontWeight: FontWeight.bold);
}
