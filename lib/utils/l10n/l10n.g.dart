// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'l10n.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$localizationsHash() => r'f64e4dfcb8693baaae6b9950471f94064ae9b3e8';

/// See also [Localizations].
@ProviderFor(Localizations)
final localizationsProvider =
    AutoDisposeNotifierProvider<Localizations, Locale>.internal(
  Localizations.new,
  name: r'localizationsProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$localizationsHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Localizations = AutoDisposeNotifier<Locale>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
