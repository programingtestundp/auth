import 'package:flutter/material.dart' show Locale;
import 'package:riverpod_annotation/riverpod_annotation.dart';
part 'l10n.g.dart';

@riverpod
class Localizations extends _$Localizations {
  @override
  Locale build() => const Locale('en');

  final all = [
    const Locale('en'),
    const Locale('ar'),
  ];
}
