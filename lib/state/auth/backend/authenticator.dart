import 'package:auth/extensions/object/index.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../../user_info/models/user.dart';
import '../constants/constants.dart';
import '../models/auth_result.dart';

class Authenticator {
  // ▶ for testing ◀ //
  FirebaseAuth firebaseI;
  Authenticator({FirebaseAuth? firebaseI})
      : firebaseI = firebaseI ?? FirebaseAuth.instance;
  FirebaseAuth get firebaseInstance => firebaseI;
  User? get currentUser => firebaseInstance.currentUser;
  String? get userId => currentUser?.uid;
  bool get isAlreadyLogin => userId != null;
  String get displayName => currentUser?.displayName ?? '';
  String get email => currentUser?.email ?? '';

  Future<void> logout() async {
    try {
      await firebaseInstance.signOut();
      await GoogleSignIn().signOut();
    } on Exception catch (e) {
      e.log();
    }
// await FacebookAuth.instance.signOut();
  }

  Future<({AuthResults results, String message})> loginAnonymously() async {
    try {
      final UserCredential userCredential =
          await firebaseInstance.signInAnonymously();

      userCredential.log();
      return (results: AuthResults.success, message: '');
    } on FirebaseAuthException catch (e) {
      return (
        results: AuthResults.failure,
        message: "Error-Login:${e.message?.split('.')[0]}"
      );
    }
  }

  Future<({AuthResults results, String message, UserModel? googleUser})>
      loginWithGoogle(
          {GoogleSignIn? mockGoogleSignIn,
          OAuthCredential? mockOauthCredentials}) async {
    final GoogleSignIn googleSignIn =
        mockGoogleSignIn ?? GoogleSignIn(scopes: [Constants.emailScope]);

    final signInAccount = await googleSignIn.signIn();
    if (signInAccount == null) {
      return (results: AuthResults.aborted, message: '', googleUser: null);
    }

    final googleAuth = await signInAccount.authentication;

    final oauthCredentials = mockOauthCredentials ??
        GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);

    try {
      final UserCredential userCredential =
          await firebaseInstance.signInWithCredential(oauthCredentials);

      final user = UserModel(
          email: signInAccount.email,
          imageUrl: signInAccount.photoUrl ?? "",
          name: signInAccount.displayName ?? "");
      userCredential.log();
      return (results: AuthResults.success, message: '', googleUser: user);
    } on FirebaseAuthException catch (e) {
      // final email = e.email;
//       final credential = e.credential;
//       if (e.code == Constants.accountExistsWithDifferentCredential &&
//           email != null &&
//           credential != null) {
//         //!search alternative to check if the user login with other platform
//         final providers =
//             await FirebaseAuth.instance.fetchSignInMethodsForEmail(
//           email,
//         );
//         if (providers.contains(Constants.facebookCom)) {
// // await loginWithFacebook();
//           currentUser?.linkWithCredential(credential);
//           return (results: AuthResults.success, message: '');
//         }
//       }
      // currentUser.linkWithCredential(credential)
      return (
        googleUser: null,
        results: AuthResults.failure,
        message: "Error-Login With Google:${e.message?.split('.')[0]}"
      );
    }
  }

  Future<({AuthResults results, String message})> loginWithEmail(
      UserModel user, String passWord) async {
    try {
      final UserCredential userCredential = await firebaseInstance
          .signInWithEmailAndPassword(email: user.email, password: passWord);

      userCredential.log();
      return (results: AuthResults.success, message: '');
    } on FirebaseAuthException catch (e) {
      return (
        results: AuthResults.failure,
        message: e.code == 'invalid-credential'
            ? "The password is not correct"
            : "Error-Login:${e.message?.split('.')[0]}"
      );
    }
  }

  Future<({AuthResults results, String message})> signUpWithEmail(
      UserModel user, String passWord) async {
    try {
      if (user.email.isEmpty || passWord.isEmpty) {
        return (
          results: AuthResults.failure,
          message: "Error-Login:password or email is empty"
        );
      }

      final UserCredential userCredential =
          await firebaseInstance.createUserWithEmailAndPassword(
              email: user.email, password: passWord);

      userCredential.log();

      return (results: AuthResults.success, message: '');
    } on FirebaseAuthException catch (e) {
      return (
        results: AuthResults.failure,
        message: "Error-Login:${e.message?.split('.')[0]}"
      );
    }
  }
}
