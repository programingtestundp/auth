// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart' show immutable;

import '../../user_info/models/user.dart';
import 'auth_result.dart';

@immutable
class AuthState extends Equatable {
  final AuthResults? result;
  final bool isLoading;

  final UserModel? user;

  const AuthState({
    required this.user,
    required this.isLoading,
    required this.result,
  });

  const AuthState.unknown()
      : user = null,
        result = null,
        isLoading = false;

  AuthState copiedWithIsLoading(bool isLoading) =>
      AuthState(result: result, isLoading: isLoading, user: user);

  @override
  List<Object?> get props => [result, isLoading, user];
}
