import 'package:auth/extensions/string/index.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:phone_number/phone_number.dart';

class Validations {
  final String _regExp = r'(?=.*?[!@#$£%^&*(\)-=+\{\}\[\]:<>,.?/;\"])';
  final String _regExPassword =
      r'^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()_+{}:"<>?,./;])';

  MultiValidator nameValidations({
    int max = 15,
    int min = 3,
    String? extraPattern,
    List<String>? pannedWords,
    bool? externalError,
    String externalErrorString = 'externalError',
  }) {
    pannedWords ??= [];
    // pannedWords.add(regExp);
    return MultiValidator([
      RequiredValidator(errorText: "Required Text"),
      NameValidation(errorText: 'Invalid Name'),
      MaxLengthValidator(max, errorText: "Name cannot be more 15 characters"),
      MinLengthValidator(min,
          errorText: "Name must contain at least 3 characters"),
      if (extraPattern != null)
        PatternValidator(_regExp + extraPattern, errorText: "Invalided Name"),
      PannedWords(pannedWords: pannedWords, errorText: "Panned Name"),
      if (externalError != null)
        ExternalValidator(
            errorText: externalErrorString, externalError: externalError)
    ]);
  }

  MultiValidator emailValidations({
    String? extraPattern,
    List<String>? pannedWords,
    bool? externalError,
    String externalErrorString = 'externalError',
  }) {
    pannedWords ??= [];
    // pannedWords.add(regExp);
    return MultiValidator([
      RequiredValidator(errorText: "Required Text"),
      EmailValidator(errorText: 'Invalid Email'),
      ExtraEmailValidation(errorText: 'Invalid Email'),
      if (extraPattern != null)
        PatternValidator(_regExp + extraPattern, errorText: "Invalided Name"),
      PannedWords(pannedWords: pannedWords, errorText: "Panned Name"),
      if (externalError != null)
        ExternalValidator(
            errorText: externalErrorString, externalError: externalError)
    ]);
  }

  MultiValidator passwordValidations({
    int max = 15,
    int min = 6,
    String? extraPattern,
    String? confirmPassword,
    List<String>? pannedWords,
    bool? externalError,
    String externalErrorString = 'externalError',
  }) {
    pannedWords ??= [];
    extraPattern ??= '';
    return MultiValidator([
      MaxLengthValidator(max,
          errorText: "Password cannot be more 15 characters"),
      MinLengthValidator(min,
          errorText: "Password must contain at least 6 characters"),
      PatternValidator(_regExPassword,
          errorText: "Password has capital letter and symbols and numbers"),
      PannedWords(pannedWords: pannedWords, errorText: "Panned Password"),
      if (confirmPassword != null)
        MatchingValidator(
            errorText: 'Wrong Password', otherText: confirmPassword),
      if (externalError != null)
        ExternalValidator(
            errorText: externalErrorString, externalError: externalError)
    ]);
  }
}

class PannedWords extends TextFieldValidator {
  final List<Pattern> pannedWords;
  @override
  bool get ignoreEmptyValues => false;

  PannedWords({required this.pannedWords, required String errorText})
      : super(errorText);

  @override
  bool isValid(String? value) {
    List<String> inputWords = value!.removeExtraSpace().split(' ').toList();
    bool checkKey = true;
    for (var word in pannedWords) {
      checkKey = checkKey &&
          !inputWords.any((element) =>
              hasMatch(word.toString(), element, caseSensitive: false));
    }
    return checkKey;
  }
}

class NameValidation extends TextFieldValidator {
  @override
  bool get ignoreEmptyValues => false;

  NameValidation({required String errorText}) : super(errorText);
  @override
  bool isValid(String? value) {
    String regExp = r'^[a-zA-Z ]+$';

    return hasMatch(regExp, value!, caseSensitive: false);
  }
}

class UserNameValidation extends TextFieldValidator {
  @override
  bool get ignoreEmptyValues => false;

  UserNameValidation({required String errorText}) : super(errorText);
  @override
  bool isValid(String? value) {
    String regExp = r'^[a-zA-Z0-9]+$';

    return hasMatch(regExp, value!, caseSensitive: false);
  }
}

class ExtraEmailValidation extends TextFieldValidator {
  @override
  bool get ignoreEmptyValues => false;

  ExtraEmailValidation({required String errorText}) : super(errorText);
  @override
  bool isValid(String? value) {
    String regExp = r'^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]+$';

    return hasMatch(regExp, value!, caseSensitive: false);
  }
}

Future<bool> isValidPhoneNumber({required String phoneNumberText}) async {
  // String phoneNumber = phoneNumberText;
  PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil();
  try {
    // PhoneNumber number = await phoneNumberUtil.parse(phoneNumber);
    return await phoneNumberUtil.validate(phoneNumberText);
  } catch (e) {
    return false;
  }
}

class MatchingValidator extends TextFieldValidator {
  @override
  final String errorText;
  final String otherText;

  MatchingValidator({required this.errorText, required this.otherText})
      : super(errorText);

  @override
  bool isValid(String? value) {
    return value == otherText;
  }
}

class ExternalValidator extends TextFieldValidator {
  @override
  final String errorText;
  final bool externalError;

  ExternalValidator({required this.errorText, required this.externalError})
      : super(errorText);

  @override
  bool isValid(String? value) {
    return !externalError;
  }
}
