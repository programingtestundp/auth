import 'package:flutter/services.dart';
import 'package:phone_number/phone_number.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'phone_validation_provider.g.dart';

@riverpod
class ValidatePhone extends _$ValidatePhone {
  @override
  String build() => '';
  set setState(String state) => this.state = state;

  Future<String> isValidPhoneNumber(
      String phoneNumberString, String regionCode) async {
    try {
      PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil();

      final isValid = await phoneNumberUtil.validate(phoneNumberString,
          regionCode: regionCode.toUpperCase());
      state = isValid ? '' : 'Invalid Number';
    } on PlatformException catch (e) {
      state = e.message ?? 'Error';
    } on Exception catch (e) {
      state = e.toString();
    }

    return state;
  }
}
