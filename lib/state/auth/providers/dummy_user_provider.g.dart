// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dummy_user_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$userHash() => r'1a46c43377b0e2016538d050de0d4987cf7f4eef';

/// See also [User].
@ProviderFor(User)
final userProvider = AutoDisposeNotifierProvider<User, UserModel>.internal(
  User.new,
  name: r'userProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$userHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$User = AutoDisposeNotifier<UserModel>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
