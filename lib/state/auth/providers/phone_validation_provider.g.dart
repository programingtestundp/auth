// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_validation_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$validatePhoneHash() => r'b47265f2d1f3bea53371729a740bc679c79c0d78';

/// See also [ValidatePhone].
@ProviderFor(ValidatePhone)
final validatePhoneProvider =
    AutoDisposeNotifierProvider<ValidatePhone, String>.internal(
  ValidatePhone.new,
  name: r'validatePhoneProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$validatePhoneHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ValidatePhone = AutoDisposeNotifier<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
