// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'password_prvider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$passwordHash() => r'eeb58d53dce2c357b6e4fc3c36a89a242f8c61a7';

/// See also [Password].
@ProviderFor(Password)
final passwordProvider = AutoDisposeNotifierProvider<Password, String>.internal(
  Password.new,
  name: r'passwordProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$passwordHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Password = AutoDisposeNotifier<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
