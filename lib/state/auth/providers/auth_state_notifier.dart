import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:sqflite/sqflite.dart' show Database;

import '../../providers/ui providers/message_provider.dart';
import '../../user_info/DB/user_info_db.dart';
import '../../user_info/backend/user_info_firestore_storge.dart';
import '../../user_info/models/user.dart';
import '../backend/authenticator.dart';
import '../models/auth_result.dart';
import '../models/auth_state.dart';
import 'dummy_user_provider.dart';
import 'password_prvider.dart';
part 'auth_state_notifier.g.dart';

@Riverpod(keepAlive: true)
class AuthNotifier extends _$AuthNotifier {
  AuthNotifier();

  // ▶ named constructor for mocking ◀ //
  factory AuthNotifier.test(
      {FirebaseAuth? fAuth, FirebaseFirestore? fireStore, Database? database}) {
    final AuthNotifier authNotifier = AuthNotifier();
    authNotifier.authenticator = Authenticator(firebaseI: fAuth);
    authNotifier.userFireStore =
        UserInfoFireStorageStorage(fireStore: fireStore);
    authNotifier.userSqlDB = UserInfoSQLStorage(database: database);
    return authNotifier;
  }
  Authenticator authenticator = Authenticator();
  UserInfoFireStorageStorage userFireStore = UserInfoFireStorageStorage();
  UserInfoSQLStorage userSqlDB = UserInfoSQLStorage();
  @override
  AuthState build() {
    if (authenticator.isAlreadyLogin) {
      final currentUser = authenticator.currentUser!;

      final user = UserModel.fromFirebaseUser(currentUser);
      return AuthState(
          user: user, isLoading: false, result: AuthResults.success);
    } else {
      return const AuthState.unknown();
    }
  }

  Future<bool> setUser() async {
    // ▶ get user from the aql db ◀ //
    final result = await userSqlDB.getUser();
    ;

    if (result is Map<String, dynamic>) {
      state = AuthState(
          user: UserModel.fromMap(result),
          isLoading: false,
          result: state.result);
    }
    return true;
  }

  Future<AuthResults> loginAnonymously() async {
    state = state.copiedWithIsLoading(true);
    final res = await authenticator.loginAnonymously();

    if (res.results == AuthResults.failure) {
      return _returnError(message: res.message);
    }

    state = const AuthState(
        user: null, isLoading: false, result: AuthResults.success);
    return AuthResults.success;
  }

  Future<void> logout() async {
    state = state.copiedWithIsLoading(true);
    await userSqlDB.deleteUser();
    await authenticator.logout();

    state = const AuthState.unknown();
  }

// ▶ login with email ◀ //
  Future<AuthResults> signUp({UserModel? user, String? passWord}) async {
    // ▶ set State as loading  ◀ //
    state = state.copiedWithIsLoading(true);
    user ??= ref.read(userProvider)!;

    // ▶ check the date ◀ //
    if (user.birthDay.isEmpty || user.birthDay == '0000/00//00') {
      // ▶ error ◀ //
      return _returnError(message: 'Provide birthDay');
    }
    const validatePhone = '';

    // await ref
    //     .read(validatePhoneProvider.notifier)
    //     .isValidPhoneNumber(user.phone.number, user.phone.code);
    if (validatePhone.isNotEmpty) {
      // ▶ error ◀ //
      return _returnError(message: validatePhone);
    }

    // ▶ check password ◀ //
    passWord ??= ref.read(passwordProvider)!;
    final (results: result, message: message) =
        await authenticator.signUpWithEmail(user, passWord);
    if (result == AuthResults.failure) {
      return _returnError(
        message: message,
      );
    }
    final currentUser = authenticator.currentUser;
    if (currentUser == null) {
      return _returnError(
        message: 'currentUser = null ~ signUp <fn>',
      );
    }

    return await _saveUserInfo(
        user: user.updateFromFirebaseUser(currentUser, logType: 'email'),
        result: result,
        message: message);
  }

// ▶ login with email ◀ //
  Future<AuthResults> logInWithEmail(
      {UserModel? user, String? passWord}) async {
    // ▶ set State as loading  ◀ //
    state = state.copiedWithIsLoading(true);

    passWord ??= ref.read(passwordProvider)!;

    user ??= ref.read(userProvider)!;

    if (user.email.isEmpty || passWord.isEmpty) {
      // ▶ password or email error ◀ //
      return _returnError(message: 'Error-Login:password or email is empty');
    }
    // ▶ check if the user exists ◀ //
    final getUserRes = await userFireStore.getUser(user.email);

    if (getUserRes.results == AuthResults.failure) {
      // ▶ if the user doesn't exists ◀ //
      return _returnError(message: getUserRes.message);
    }

    final (results: result, message: message) =
        await authenticator.loginWithEmail(user, passWord);

    if (result == AuthResults.failure) {
      // ▶ invalid credential error ◀ //
      return _returnError(message: message);
    }
    // ▶ if the currentUser == null ◀ //
    final currentUser = authenticator.currentUser;
    if (currentUser == null) {
      return _returnError(
        message: 'currentUser = null ~ signUp <fn>',
      );
    }
    user =
        getUserRes.user!.updateFromFirebaseUser(currentUser, logType: 'email');
    return await _saveUserInfo(user: user, result: result, message: message);
  }

  Future<AuthResults> logInWithGoogle(
      {GoogleSignIn? mockGoogleSignIn,
      OAuthCredential? mockOauthCredentials}) async {
    // ▶ set State as loading  ◀ //
    state = state.copiedWithIsLoading(true);

    final (results: result, message: message, googleUser: googleUser) =
        await authenticator.loginWithGoogle(
            mockGoogleSignIn: mockGoogleSignIn,
            mockOauthCredentials: mockOauthCredentials);

    final currentUser = authenticator.currentUser;

    if (result == AuthResults.aborted) {
      return _returnError(
        message: message,
      );
    }
    if (result == AuthResults.failure) {
      return _returnError(
        message: message,
      );
    }
    if (currentUser == null) {
      return _returnError(
        message: 'currentUser = null ~ signUp <fn>',
      );
    }

    final getUserRes = await userFireStore.getUser(googleUser!.email);
    if (getUserRes.results == AuthResults.failure) {
      if (message == 'The account is not exists please SignUp') {
        // ▶ if the user doesn't exists then create 1 ◀ //
        return await _saveUserInfo(
            user: UserModel().updateFromFirebaseUser(currentUser),
            result: result,
            message: message);
      } else {
        return _returnError(message: message);
      }
    }
    // ▶ if the user is exists ◀ //
    final user =
        getUserRes.user!.updateFromFirebaseUser(currentUser, logType: 'google');
    return await _saveUserInfo(
        user: user, result: result, message: message, logType: 'google');
  }

// ▶ check and save the user info to firebase database ◀ //
  Future<AuthResults> _saveUserInfo(
      {required UserModel user,
      String? logType,
      required AuthResults result,
      required String message}) async {
    if (result == AuthResults.success) {
      // ▶  save the user in fire store ◀ //
      String res =
          await userFireStore.saveUserInfoToFireStore(user: user, byId: false);

      if (res != 'success') {
        // ▶ error with save user in fire store ◀ //
        return _returnError(message: res);
      }
      // ▶ return the final results ◀ //
      return await _saveUserInfoInDB(user);
    }
    state = AuthState(user: user, isLoading: false, result: result);
    return result;
  }

// ▶ save the user in the SQL Db ◀ //
  Future<AuthResults> _saveUserInfoInDB(UserModel user) async {
    String result = await userSqlDB.deleteUser();

    if (result != 'success') {
      return _returnError(
        message: result,
      );
    }
    result = await userSqlDB.createUser(user);
    if (result != 'success') {
      return _returnError(
        message: result,
      );
    }
    state =
        AuthState(user: user, isLoading: false, result: AuthResults.success);
    return AuthResults.success;
  }

  // ▶ return error ◀ //
  AuthResults _returnError({required String message, UserModel? user}) {
    state =
        AuthState(user: user, isLoading: false, result: AuthResults.failure);
    // ▶ to print error message in snake bar  ◀ //
    ref.read(messageProvider.notifier).state = message;
    return AuthResults.failure;
  }
}
