import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../user_info/models/user.dart';

part 'dummy_user_provider.g.dart';

// ▶ use to fill user model with dummy data for registration or edit ◀ //
@riverpod
class User extends _$User {
  @override
  UserModel build() => UserModel();
  set setState(UserModel state) => this.state = state;
  resetState() => state = UserModel();
  setUserData({
    String? id,
    String? email,
    String? name,
    String? countryId,
    String? imageUrl,
    String? imagePath,
    String? logType,
    String? birthDay,
    String? gender,
    // ▶ phone ◀ //
    String? code,
    String? number,
    String? dialCode,
    // ▶ address ◀ //
    String? street,
    String? city,
    String? country,
  }) =>
      state = state.copyWith(
          email: email,
          name: name,
          countryId: countryId,
          imageUrl: imageUrl,
          imagePath: imagePath,
          phone: state.phone
              .copyWith(code: code, dialCode: dialCode, number: number),
          address: state.address
              .copyWith(street: street, city: city, country: country),
          logType: logType,
          gender: gender,
          birthDay: birthDay);
}
