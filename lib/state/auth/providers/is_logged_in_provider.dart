import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../models/auth_result.dart';
import 'auth_state_notifier.dart';
part 'is_logged_in_provider.g.dart';

@riverpod
Future<bool> isLoggedIn(IsLoggedInRef ref) async {
  final authState = ref.watch(authNotifierProvider);
  return authState.result == AuthResults.success;
}
