import 'package:riverpod_annotation/riverpod_annotation.dart';
part 'password_prvider.g.dart';

@riverpod
class Password extends _$Password {
  bool confirmed = false;
  @override
  String build() => '';
  set setState(String state) => this.state = state;
  set confirm(String state) => confirmed = this.state == state;
  bool get isConfirmed => confirmed;
  void resetState() {
    state = '';
    confirmed = false;
  }
}
