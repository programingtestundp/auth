import 'package:flutter/foundation.dart' show immutable;

@immutable
class FirebaseCollectionsNames {
  static const users = 'users';
  static const address = 'address';
  static const phone = 'phone';
  const FirebaseCollectionsNames._();
}
