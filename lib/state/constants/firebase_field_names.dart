import 'package:flutter/foundation.dart' show immutable;

@immutable
class FirebaseFieldsNamesUsers {
  static const userId = 'uid';
  static const email = 'email';
  static const name = 'name';
  static const imageUrl = 'image_url';
  static const imagePath = 'image_path';
  static const phonNumber = 'phon_number';
  static const logType = 'log_type';
  static const birthDay = 'birth_day';
  static const gender = 'gender';
  // ▶ address ◀ //
  static const street = 'street';
  static const city = 'city';
  static const country = 'country';
  // ▶ country ◀ //
  static const code = 'code;';
  static const number = 'number';
  static const dialCode = 'dialCode';

  const FirebaseFieldsNamesUsers._();
}
