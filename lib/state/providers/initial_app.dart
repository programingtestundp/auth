import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../auth/providers/auth_state_notifier.dart';

part 'initial_app.g.dart';

@riverpod
Future<bool> initialApp(InitialAppRef ref) async {
  await ref.read(authNotifierProvider.notifier).setUser();

  return true;
}
