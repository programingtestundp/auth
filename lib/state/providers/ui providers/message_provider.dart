import 'package:auth/extensions/context/index.dart';
import 'package:flutter/material.dart' show BuildContext, Color, Colors;
import 'package:riverpod_annotation/riverpod_annotation.dart';
part 'message_provider.g.dart';

@riverpod
class Message extends _$Message {
  @override
  String build() => '';
  trigger(
    BuildContext context, {
    Color backgroundColor = Colors.black,
    Color textColor = Colors.white,
    int duration = 7000,
  }) {
    context.showSnackBar(
        message: state,
        backgroundColor: backgroundColor,
        duration: duration,
        textColor: textColor);
    state = '';
  }
}
