// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'button_tate_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$buttonStateHash() => r'5f1600cb360a413597817a01237a91223a483a93';

/// See also [ButtonState].
@ProviderFor(ButtonState)
final buttonStateProvider =
    AutoDisposeNotifierProvider<ButtonState, ButtonStatus>.internal(
  ButtonState.new,
  name: r'buttonStateProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$buttonStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ButtonState = AutoDisposeNotifier<ButtonStatus>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
