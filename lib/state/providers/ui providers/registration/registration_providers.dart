import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../../auth/providers/dummy_user_provider.dart';
import '../../../auth/providers/password_prvider.dart';
import 'initial_registration_proviser.dart';

part 'registration_providers.g.dart';

@riverpod
class SwitchRegistrationPage extends _$SwitchRegistrationPage {
  @override
  build() => 0;
  set setState(int state) {
    if (state != this.state) {
      ref.read(passwordProvider.notifier).resetState();
      // ▶ dummy user ◀ //
      final (
        localRegionCode: localRegionCode,
        selectedCountry: selectedCountry
      ) = ref.read(initialRegistrationValuesProvider);
      ref.read(userProvider.notifier).setUserData(
          code: localRegionCode,
          dialCode: "US",
          country: selectedCountry!.name);
    }
    this.state = state;
  }
}

@riverpod
class RememberMeKey extends _$RememberMeKey {
  @override
  bool build() => false;
  set setState(bool state) => this.state = state;
}
