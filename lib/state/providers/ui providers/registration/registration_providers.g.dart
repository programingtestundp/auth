// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registration_providers.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$switchRegistrationPageHash() =>
    r'f91a1fbda314a229c0207f6215220d97479d80a8';

/// See also [SwitchRegistrationPage].
@ProviderFor(SwitchRegistrationPage)
final switchRegistrationPageProvider =
    AutoDisposeNotifierProvider<SwitchRegistrationPage, Object?>.internal(
  SwitchRegistrationPage.new,
  name: r'switchRegistrationPageProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$switchRegistrationPageHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SwitchRegistrationPage = AutoDisposeNotifier<Object?>;
String _$rememberMeKeyHash() => r'8ba21e7b762cb22bf260f8d32d7babb1b19ce365';

/// See also [RememberMeKey].
@ProviderFor(RememberMeKey)
final rememberMeKeyProvider =
    AutoDisposeNotifierProvider<RememberMeKey, bool>.internal(
  RememberMeKey.new,
  name: r'rememberMeKeyProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$rememberMeKeyHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$RememberMeKey = AutoDisposeNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
