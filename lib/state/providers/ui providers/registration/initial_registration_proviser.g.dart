// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'initial_registration_proviser.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$initialRegistrationHash() =>
    r'51a8a91db5e2eaea190d369caefd01a535577d31';

/// See also [initialRegistration].
@ProviderFor(initialRegistration)
final initialRegistrationProvider = AutoDisposeFutureProvider<bool>.internal(
  initialRegistration,
  name: r'initialRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$initialRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef InitialRegistrationRef = AutoDisposeFutureProviderRef<bool>;
String _$initialRegistrationValuesHash() =>
    r'f180e8cfa058748b4f71948f38e7305b4201a965';

/// See also [InitialRegistrationValues].
@ProviderFor(InitialRegistrationValues)
final initialRegistrationValuesProvider = AutoDisposeNotifierProvider<
    InitialRegistrationValues,
    ({String localRegionCode, Country? selectedCountry})>.internal(
  InitialRegistrationValues.new,
  name: r'initialRegistrationValuesProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$initialRegistrationValuesHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$InitialRegistrationValues
    = AutoDisposeNotifier<({String localRegionCode, Country? selectedCountry})>;
String _$autoValidateTextFieldHash() =>
    r'2d8f5969f7a4ca90fcd791f2a04920c8144d004b';

/// See also [AutoValidateTextField].
@ProviderFor(AutoValidateTextField)
final autoValidateTextFieldProvider =
    AutoDisposeNotifierProvider<AutoValidateTextField, bool>.internal(
  AutoValidateTextField.new,
  name: r'autoValidateTextFieldProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$autoValidateTextFieldHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AutoValidateTextField = AutoDisposeNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
