import 'package:intl_phone_field/countries.dart' show Country, countries;
import 'package:phone_number/phone_number.dart' show PhoneNumberUtil;
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../../../DB/registration_local_data.dart';

part 'initial_registration_proviser.g.dart';

@riverpod
Future<bool> initialRegistration(InitialRegistrationRef ref) async {
// ▶ get data for sign up page ◀ //
  final PhoneData phoneData = PhoneData(plugin: PhoneNumberUtil());
  final localRegionCode = (await phoneData.carrierRegionCode()) ?? 'US';
  ref.watch(initialRegistrationValuesProvider.notifier).setState =
      localRegionCode;

  return localRegionCode.isNotEmpty;
}

@riverpod
class InitialRegistrationValues extends _$InitialRegistrationValues {
  @override
  ({String localRegionCode, Country? selectedCountry}) build() => (
        localRegionCode: 'US',
        selectedCountry:
            countries.where((element) => element.code == "US").first
      );
  set setState(String localRegionCode) => state = (
        localRegionCode: localRegionCode,
        selectedCountry: countries
            .where((element) => element.code == localRegionCode.toUpperCase())
            .first
      );
}

@riverpod
class AutoValidateTextField extends _$AutoValidateTextField {
  @override
  bool build() => false;
  set setState(bool state) => this.state = state;
}
