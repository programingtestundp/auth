// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$messageHash() => r'02ae4eb11bcd8ff98084dc035825f46fbe0953e0';

/// See also [Message].
@ProviderFor(Message)
final messageProvider = AutoDisposeNotifierProvider<Message, String>.internal(
  Message.new,
  name: r'messageProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$messageHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Message = AutoDisposeNotifier<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
