import 'package:flutter/material.dart' show debugPrint;
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../auth/models/auth_result.dart';
part 'button_tate_provider.g.dart';

enum ButtonStatus { non, loading, done, error, canceled, animate }

@riverpod
class ButtonState extends _$ButtonState {
  ButtonState();
  factory ButtonState.initial({ButtonStatus status = ButtonStatus.non}) {
    final buttonState = ButtonState();
    buttonState.status = status;
    return buttonState;
  }
  ButtonStatus status = ButtonStatus.non;
  @override
  ButtonStatus build() => status;

  setStatus(
      {ButtonStatus? status,
      AuthResults? results,
      required int duration}) async {
    if (results == null && status != null) {
      state = status;
    } else if (results != null && status == null) {
      state = switch (results) {
        AuthResults.success => ButtonStatus.done,
        AuthResults.aborted => ButtonStatus.canceled,
        AuthResults.failure => ButtonStatus.error,
      };

      await Future.delayed(
        Duration(milliseconds: state == ButtonStatus.canceled ? 300 : 2000),
      );
      state = ButtonStatus.animate;
      await Future.delayed(Duration(milliseconds: duration));
      state = ButtonStatus.non;
    } else {
      debugPrint('setStatus<>fn: error with inputs');
    }
  }
}
