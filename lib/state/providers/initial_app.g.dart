// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'initial_app.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$initialAppHash() => r'012926a190e4cb9b59c3abd47828bde131d393bf';

/// See also [initialApp].
@ProviderFor(initialApp)
final initialAppProvider = AutoDisposeFutureProvider<bool>.internal(
  initialApp,
  name: r'initialAppProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$initialAppHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef InitialAppRef = AutoDisposeFutureProviderRef<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
