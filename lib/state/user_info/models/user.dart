// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'dart:collection';
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart' show User;
import 'package:flutter/foundation.dart' show immutable;
import 'package:uuid/uuid.dart';

import '../../constants/firebase_field_names.dart';

@immutable
class UserModel extends MapView<String, String> with EquatableMixin {
  final String id;
  final String email;
  final String name;
  final String imageUrl;
  final String imagePath;
  final Phone phone;
  final Address address;
  final String logType;
  final String birthDay;
  final String gender;
  final DateTime lastUpdate;
  UserModel(
      {this.id = '',
      this.email = '',
      this.name = '',
      this.imageUrl = '',
      this.imagePath = '',
      this.logType = '',
      this.birthDay = '0000/00//00',
      this.gender = 'Male',
      Phone? phone,
      Address? address,
      DateTime? lastUpdate,
      // ▶ if the model used for update (normal user) or for add new element to DB ◀ //
      bool isNew = false})
      : address = address ?? Address(),
        lastUpdate = lastUpdate ?? DateTime.now(),
        phone = phone ?? Phone(),
        super({
          if (!isNew && id != '') FirebaseFieldsNamesUsers.userId: id,
          if (!isNew && email != '') FirebaseFieldsNamesUsers.email: email,
          if (!isNew && name != '') FirebaseFieldsNamesUsers.name: name,
          if (!isNew && imageUrl != '')
            FirebaseFieldsNamesUsers.imageUrl: imageUrl,
          if (!isNew && imagePath != '')
            FirebaseFieldsNamesUsers.imagePath: imagePath,
          if (!isNew && logType != '')
            FirebaseFieldsNamesUsers.logType: logType,
          if (!isNew && birthDay != '')
            FirebaseFieldsNamesUsers.birthDay: birthDay,
          if (!isNew && gender != '') FirebaseFieldsNamesUsers.gender: gender,
        });
  UserModel copyWith({
    String? id,
    String? email,
    String? name,
    String? countryId,
    String? imageUrl,
    String? imagePath,
    Phone? phone,
    Address? address,
    String? logType,
    String? birthDay,
    String? gender,
    DateTime? lastUpdate,
  }) {
    return UserModel(
      id: id ?? this.id,
      email: email ?? this.email,
      name: name ?? this.name,
      imageUrl: imageUrl ?? this.imageUrl,
      imagePath: imagePath ?? this.imagePath,
      phone: phone ?? this.phone,
      address: address ?? this.address,
      logType: logType ?? this.logType,
      birthDay: birthDay ?? this.birthDay,
      gender: gender ?? this.gender,
      lastUpdate: lastUpdate ?? this.lastUpdate,
    );
  }

// ▶ create a new instance from firebase user ◀ //
  factory UserModel.fromFirebaseUser(User user, {String? logType}) => UserModel(
      id: user.uid,
      email: user.email ?? '',
      name: user.displayName ?? 'unknown',
      imageUrl: user.photoURL ?? '',
      logType: logType ?? "",
      phone: Phone(number: user.phoneNumber ?? ''));

  // ▶ update the current instance from firebase user ◀ //
  UserModel updateFromFirebaseUser(User user, {String? logType}) => copyWith(
      id: user.uid,
      email: user.email,
      name: user.displayName,
      imageUrl: user.photoURL,
      logType: logType,
      phone: user.phoneNumber != null
          ? phone.copyWith(number: user.phoneNumber)
          : phone);

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'email': email,
      'name': name,
      'imageUrl': imageUrl,
      'imagePath': imagePath,
      'phone': phone.toMap(),
      "address": address.toMap(),
      'logType': logType,
      'birthDay': birthDay,
      'gender': gender,
      "lastUpdate": lastUpdate.toIso8601String()
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
        id: (map['id'] ?? const Uuid().v4()) as String,
        email: (map['email'] ?? '') as String,
        name: (map['name'] ?? '') as String,
        imageUrl: (map['imageUrl'] ?? '') as String,
        imagePath: (map['imagePath'] ?? "") as String,
        phone: Phone.fromMap(map['phone'] ?? {}),
        address: Address.fromMap(map['address'] ?? {}),
        logType: (map['logType'] ?? "") as String,
        birthDay: (map['birthDay'] ?? "") as String,
        gender: (map['gender'] ?? "") as String,
        lastUpdate: map['lastUpdate'] != null
            ? DateTime.fromMillisecondsSinceEpoch(map['lastUpdate'] as int)
            : DateTime.now());
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  List<Object> get props {
    return [
      id,
      email,
      name,
      imageUrl,
      imagePath,
      phone,
      logType,
      birthDay,
      lastUpdate,
      gender
    ];
  }
}

class Address extends MapView<String, String> with EquatableMixin {
  final String id;
  final String street;

  final String city;

  final String country;

  Address({
    String? id,
    this.street = '',
    this.city = '',
    this.country = '',
  })  : id = id ?? const Uuid().v4(),
        super({
          FirebaseFieldsNamesUsers.street: street,
          FirebaseFieldsNamesUsers.city: city,
          FirebaseFieldsNamesUsers.country: country,
        });

  @override
  List<Object?> get props => [street, city, country, id];

  Address copyWith({
    String? id,
    String? street,
    String? city,
    String? country,
  }) {
    return Address(
      id: id ?? this.id,
      street: street ?? this.street,
      city: city ?? this.city,
      country: country ?? this.country,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'street': street,
      'city': city,
      'country': country,
    };
  }

  factory Address.fromMap(Map<String, dynamic> map) {
    return Address(
      id: (map['id'] ?? const Uuid().v4()) as String,
      street: (map['street'] ?? "") as String,
      city: (map['city'] ?? "") as String,
      country: (map['country'] ?? "") as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory Address.fromJson(String source) =>
      Address.fromMap(json.decode(source) as Map<String, dynamic>);
}

class Phone extends MapView<String, String> with EquatableMixin {
  final String id;
  final String code;

  final String number;

  final String dialCode;
  Phone({
    String? id,
    this.code = '',
    this.number = '',
    this.dialCode = '',
  })  : id = id ?? const Uuid().v4(),
        super({
          FirebaseFieldsNamesUsers.code: code,
          FirebaseFieldsNamesUsers.number: number,
          FirebaseFieldsNamesUsers.dialCode: dialCode,
        });
  @override
  List<Object> get props => [id, code, number, dialCode];
  Phone copyWith({
    String? id,
    String? code,
    String? number,
    String? dialCode,
  }) {
    return Phone(
      id: id ?? this.id,
      code: code ?? this.code,
      number: number ?? this.number,
      dialCode: dialCode ?? this.dialCode,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'code': code,
      'number': number,
      'dialCode': dialCode,
    };
  }

  factory Phone.fromMap(Map<String, dynamic> map) {
    return Phone(
      id: (map['id'] ?? const Uuid().v4()) as String,
      code: (map['code'] ?? "") as String,
      number: (map['number'] ?? "") as String,
      dialCode: (map['dialCode'] ?? "") as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory Phone.fromJson(String source) =>
      Phone.fromMap(json.decode(source) as Map<String, dynamic>);
}
