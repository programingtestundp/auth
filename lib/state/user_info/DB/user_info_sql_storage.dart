import 'package:flutter/foundation.dart' show immutable;
import 'package:flutter/widgets.dart';
import 'package:sqflite/sqflite.dart';

import '../../../DB/sql_database.dart';
import 'constant/query_string.dart';
import 'constant/tables_names.dart';

@immutable
class UserInfoDB {
  const UserInfoDB._();
  static UserInfoDB get sqlDB => const UserInfoDB._();
  Future getUserFromSql({Database? database}) async {
    database ??= await SQFLiteDatabase().database;
    try {
      final results = (await database.rawQuery(SQLString.selectUser));
      if (results.isEmpty) {
        throw Exception('there is no user');
      }
      return results.first;
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    } on Exception catch (e) {
      String errorMessage = e.toString();
      return errorMessage;
    }
  }

  // ▶ create User table ◀ //
  Future<String> createUser({Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.execute(SQLString.createUser);
      return "success";
    } on DatabaseException catch (e) {
      if (e is List) {
        String errorMessage =
            'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
        return errorMessage;
      } else {
        return 'Exception with drop user';
      }
    }
  }

  // ▶ drop User table ◀ //
  Future<String> dropUser({Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.execute(SQLString.dropUser);
      return "success";
    } on DatabaseException catch (e) {
      if (e is List) {
        String errorMessage =
            'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
        return errorMessage;
      } else {
        return 'Exception with drop user';
      }
    }
  }

  // ▶ insert row to User table ◀ //
  // as dart function
  Future<String> insertUser(
      {required String id,
      required String email,
      required String name,
      required String imageUrl,
      required String imagePath,
      required String logType,
      required String birthDay,
      required String gender,
      required String address_id,
      required String phone_id,
      Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    List values = [
      id,
      email,
      name,
      imageUrl,
      imagePath,
      logType,
      birthDay,
      gender,
      address_id,
      phone_id
    ];
    try {
      await database.rawInsert(SQLString.insertUser, values);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ delete ◀ //
  Future<String> deleteUser({required String id, Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.rawDelete(SQLString.deleteUserRow, [id]);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ update User table ◀ //
  Future<String> updateUser(
      {required String id,
      required Map<String, dynamic> map,
      Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.update(TablesNames.user, map,
          where: 'id = ?',
          whereArgs: [id],
          conflictAlgorithm: ConflictAlgorithm.rollback);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ create Address table ◀ //
  Future<String> createAddress({Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.execute(SQLString.createAddress);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ drop Address table ◀ //
  Future<String> dropAddress({Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.execute(SQLString.dropAddress);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ insert row to Address table ◀ //
  // as dart function
  Future<String> insertAddress(
      {required String id,
      required String street,
      required String city,
      required String country,
      Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    List values = [id, street, city, country];
    try {
      await database.rawInsert(SQLString.insertAddress, values);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ delete row Address table ◀ //
  Future<String> deleteAddress({required String id, Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.rawDelete(SQLString.deleteAddressRow, [id]);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ update Address table ◀ //
  Future<String> updateAddress(
      {required String id,
      required Map<String, dynamic> map,
      Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.update(TablesNames.address, map,
          where: 'id = ?',
          whereArgs: [id],
          conflictAlgorithm: ConflictAlgorithm.rollback);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ create Phone table ◀ //
  Future<String> createPhone({Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.execute(SQLString.createPhone);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ drop Phone table ◀ //
  Future<String> dropPhone({Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.execute(SQLString.dropPhone);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ insert row to Phone table ◀ //
  // as dart function
  Future<String> insertPhone(
      {required String id,
      required String code,
      required String number,
      required String dialCode,
      Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    List values = [id, code, number, dialCode];
    try {
      await database.rawInsert(SQLString.insertPhone, values);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ delete row Phone table ◀ //
  Future<String> deletePhone({required String id, Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.rawDelete(SQLString.deletePhoneRow, [id]);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }

  // ▶ update Phone table ◀ //
  Future<String> updatePhone(
      {required String id,
      required Map<String, dynamic> map,
      Database? database}) async {
    database ??= await SQFLiteDatabase().database;

    try {
      await database.update(TablesNames.phone, map,
          where: 'id = ?',
          whereArgs: [id],
          conflictAlgorithm: ConflictAlgorithm.rollback);
      return "success";
    } on DatabaseException catch (e) {
      String errorMessage =
          'Code (${e.getResultCode()}): ${e.toString().split('(')[1]}';
      return errorMessage;
    }
  }
}
