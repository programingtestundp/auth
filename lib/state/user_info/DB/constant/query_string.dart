import 'package:flutter/foundation.dart' show immutable;

@immutable
class SQLString {
  // ▶ User ◀ //
  static const createUser = """CREATE TABLE IF NOT EXISTS User ( 
 id varChar(50) PRIMARY KEY,
 email varChar(50) NOT NULL,
 name varChar(50) NOT NULL,
 imageUrl varChar(200) NOT NULL,
 imagePath varChar(200) NOT NULL,
 logType varChar(10) NOT NULL,
 birthDay varChar(20) NOT NULL,
 gender varChar(10) NOT NULL,
 address_id varChar(10),
 phone_id varChar(10),
 FOREIGN KEY (address_id) REFERENCES Address (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
 FOREIGN KEY (phone_id) REFERENCES Phone (id) ON DELETE NO ACTION ON UPDATE NO ACTION)""";

  static const insertUser =
      'INSERT INTO User (id,email,name,imageUrl,imagePath,logType,birthDay,gender,address_id,phone_id) VALUES (?,?,?,?,?,?,?,?,?,?);';

  static const dropUser = 'DROP Table IF EXISTS User;';
  static const deleteUserRow = 'DELETE FROM User WHERE id= ?;';
  static const selectUser =
      """SELECT User.id AS id,User.email AS email,User.name AS name,User.imageUrl AS imageUrl,User.imagePath AS imagePath,User.logType AS logType,User.birthDay AS birthDay,User.gender AS gender,User.address_id AS address_id,User.phone_id AS phone_id,Address.id As Address_id,Address.street AS street,Address.city AS city,Address.country AS country,Phone.id As Phone_id,Phone.code AS code,Phone.number AS number,Phone.dialCode AS dialCode
FROM User
LEFT JOIN Address ON User.address_id = Address.id
LEFT JOIN Phone ON User.phone_id = Phone.id;""";

// ▶ Address ◀ //
  static const createAddress = """CREATE TABLE IF NOT EXISTS Address ( 
 id varChar(50) PRIMARY KEY,
 street varChar(20) NOT NULL,
 city varChar(20) NOT NULL,
 country varChar(20) NOT NULL)""";

  static const insertAddress =
      'INSERT INTO Address (id,street,city,country) VALUES (?,?,?,?);';
  static const dropAddress = 'DROP Table IF EXISTS Address;';
  static const deleteAddressRow = 'DELETE FROM Address WHERE id= ?;';
// ▶ Phone ◀ //
  static const createPhone = """CREATE TABLE IF NOT EXISTS Phone ( 
 id varChar(20) PRIMARY KEY,
 code varChar(10) NOT NULL,
 number varChar(20) NOT NULL,
 dialCode varChar(10) NOT NULL)""";

  static const insertPhone =
      'INSERT INTO Phone (id,code,number,dialCode) VALUES (?,?,?,?);';
  static const dropPhone = 'DROP Table IF EXISTS Phone;';
  static const deletePhoneRow = 'DELETE FROM Phone WHERE id= ?;';
}
