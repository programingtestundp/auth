import 'package:flutter/foundation.dart' show immutable;

@immutable
class TablesNames {
  static const user = 'User';
  static const address = 'Address';
  static const phone = 'Phone';
}
