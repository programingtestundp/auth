import 'package:flutter/foundation.dart' show immutable;
import 'package:sqflite/sqflite.dart' show Database;
import '../models/user.dart';
import 'user_info_sql_storage.dart';

@immutable
class UserInfoSQLStorage {
  UserInfoSQLStorage({this.database});
  final _sqlDB = UserInfoDB.sqlDB;
  // ▶ for mocking ◀ //
  final Database? database;
  Future<String> createUser(UserModel user) async {
    List<String> results = await Future.wait([
      _sqlDB.createAddress(database: database),
      _sqlDB.createPhone(database: database)
    ]);
    results.add(await _sqlDB.createUser(database: database));
    if (results.any((element) => element != 'success')) {
// ▶  there is an error ◀ //

      return "Error: ${results.where((element) => element != 'success').toList().join(",\nError: ")}";
    } else {
      results = await Future.wait([
        _sqlDB.insertAddress(
            database: database,
            id: user.address.id,
            street: user.address.street,
            city: user.address.city,
            country: user.address.country),
        _sqlDB.insertPhone(
            database: database,
            id: user.phone.id,
            code: user.phone.code,
            number: user.phone.number,
            dialCode: user.phone.dialCode)
      ]);

      results.add(await _sqlDB.insertUser(
          database: database,
          id: user.id,
          email: user.email,
          name: user.name,
          birthDay: user.birthDay,
          gender: user.gender,
          imagePath: user.imagePath,
          imageUrl: user.imageUrl,
          logType: user.logType,
          address_id: user.address.id,
          phone_id: user.phone.id));
      if (results.any((element) => element != 'success')) {
        return "Error: ${results.where((element) => element != 'success').toList().join(",\nError: ")}";
      } else {
        return 'success';
      }
    }
  }

  Future getUser() async => _sqlDB.getUserFromSql(database: database);
  Future<String> deleteUser() async {
    String result = await _sqlDB.dropUser(database: database);
    if (result != 'success') {
      return result;
    }
    result = await _sqlDB.dropAddress(database: database);
    if (result != 'success') {
      return result;
    }
    result = await _sqlDB.dropPhone(database: database);

    return result;
  }

  updateUser(UserModel user) async {
    Map<String, dynamic> userMap = {
      'id': user.id,
      'email': user.email,
      'name': user.name,
      'imageUrl': user.imageUrl,
      'imagePath': user.imagePath,
      'logType': user.logType,
      'birthDay': user.birthDay,
      'gender': user.gender,
      'address_id': user.address.id,
      'phone_id': user.phone.id
    };
    String result =
        await _sqlDB.updateUser(id: user.id, map: userMap, database: database);
    if (result != 'success') {
      return result;
    }
    // ▶ example map for update Address table ◀ //
    Map<String, dynamic> addressMap = {
      'id': user.address.id,
      'street': user.address.street,
      'city': user.address.city,
      'country': user.address.country
    };
    result = await _sqlDB.updateAddress(
        id: user.address.id, map: addressMap, database: database);
    if (result != 'success') {
      return result;
    }
// ▶ example map for update Phone table ◀ //
    Map<String, dynamic> phoneMap = {
      'id': user.phone.id,
      'code': user.phone.code,
      'number': user.phone.number,
      'dialCode': user.phone.dialCode
    };
    result = await _sqlDB.updatePhone(
        id: user.phone.id, map: phoneMap, database: database);

    return result;
  }
}
