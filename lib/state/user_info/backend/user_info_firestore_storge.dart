import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart' show immutable;

import '../../../view/screens/Auth_Screens/widgets/index.dart';
import '../../auth/models/auth_result.dart';
import '../../constants/firebase_collections_names.dart';
import '../../constants/firebase_field_names.dart';
import '../models/user.dart';

@immutable
class UserInfoFireStorageStorage {
  UserInfoFireStorageStorage({FirebaseFirestore? fireStore})
      : fireStore = fireStore ?? FirebaseFirestore.instance;
  final FirebaseFirestore fireStore;
  Future<String> saveUserInfoToFireStore(
      {required UserModel user, bool byId = true}) async {
    try {
      // ▶ get all users defacements ◀ //

      final userInfo = await fireStore
          .collection(FirebaseCollectionsNames.users)
          .where(
              byId
                  ? FirebaseFieldsNamesUsers.userId
                  : FirebaseFieldsNamesUsers.email,
              isEqualTo: byId ? user.id : user.email)
          .limit(1)
          .get();

      if (userInfo.docs.isNotEmpty) {
        // ▶ update user if is exists ◀ //
        final userRef = userInfo.docs.first.reference;

        await userRef.update(user);

        // ▶ get users-phones defacements ◀ //

        final phones =
            await userRef.collection(FirebaseCollectionsNames.phone).get();

        // ▶ get users-address defacements ◀ //
        final addresses =
            await userRef.collection(FirebaseCollectionsNames.address).get();

        // ▶ update user if is exists or create in is not ◀ //
        if (phones.docs.isNotEmpty) {
          await phones.docs.first.reference.update(user.phone);
        } else {
          userRef.collection(FirebaseCollectionsNames.phone).add(user.phone);
        }
        if (addresses.docs.isNotEmpty) {
          await addresses.docs.first.reference.update(user.address);
        } else {
          userRef
              .collection(FirebaseCollectionsNames.address)
              .add(user.address);
        }

        return 'success';
      }
      // ▶ create user with phone and address ◀ //
      final res =
          await fireStore.collection(FirebaseCollectionsNames.users).add(user);
      res.collection(FirebaseCollectionsNames.phone).add(user.phone);
      res.collection(FirebaseCollectionsNames.address).add(user.address);

      return 'success';
    } on FirebaseException catch (e) {
      return e.message ?? 'FirebaseException ~ saveUserInfo<fn>';
    } catch (e) {
      return 'Error ~ saveUserInfo<fn>';
    }
  }

// ▶ Get user by email from fire store ◀ //
  Future<({AuthResults results, String message, UserModel? user})> getUser(
      String email) async {
    // ▶ check if the user exists ◀ //
    try {
      final userInfo = await fireStore
          .collection(FirebaseCollectionsNames.users)
          .where(FirebaseFieldsNamesUsers.email, isEqualTo: email)
          .limit(1)
          .get();

      if (userInfo.docs.isEmpty) {
        // ▶ Error ◀ //
        return (
          user: null,
          results: AuthResults.failure,
          message: "The account is not exists please SignUp"
        );
      }

      final userReference = userInfo.docs.first.reference;

      final phoneInfo =
          await userReference.collection(FirebaseCollectionsNames.phone).get();

      final addressInfo = await userReference
          .collection(FirebaseCollectionsNames.address)
          .get();

      if (phoneInfo.docs.isEmpty || addressInfo.docs.isEmpty) {
        // ▶ Error ◀ //
        return (
          user: null,
          results: AuthResults.failure,
          message: "phone or address is exist but user model is not"
        );
      }

      final UserModel user = UserModel.fromMap(userInfo.docs.first.data());
      final Phone phone = Phone.fromMap(phoneInfo.docs.first.data());
      final Address address = Address.fromMap(addressInfo.docs.first.data());
      return (
        results: AuthResults.success,
        message: "",
        user: user.copyWith(phone: phone, address: address)
      );
    } on FirebaseException catch (e) {
      // if (e.code == 'permission-denied') {
      //   return (
      //     user: null,
      //     results: AuthResults.failure,
      //     message: 'You don\'t have account'
      //   );
      // }
      return (
        user: null,
        results: AuthResults.failure,
        message: e.message ?? 'Exception in getUser'
      );
    } on Exception catch (e) {
      return (user: null, results: AuthResults.failure, message: e.toString());
    }
  }
}
